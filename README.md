# Kush - KUshmaul SHell

It's not an operating system shell. It's just a utility that allows virtual commands, aliases, variable with expansion, stdin/out piping using a bash inspired syntax.
Say, for a shell-like experience within a command line utility. Or a console drop down in a game.  It has very (Very?) similar concepts but in no way
could be used as an actual shell in your favorite operating system. "It's just a tribute"

There are several main components you work with:

1. `KStdio`: proxies stdin/stdout
2. `Kush`: contains command/vars
3. `ShellCtx<T>`: - Wraps your app state, so that it can also be re-entrant within Kush.

There are also more advanced usage involving:

1. `VariableProvider`: You can look at `HashmapVariableProvider`, the default implementation
2. `CommandProvider`: Similarly, you can look at `HashmapCommandProvider` the default implementation

## Examples

Look at `examples/basic.rs` for an idea of how to create and execute the shell.

## Usage

Your basic shell concepts are:

1. Variables
2. Commands
3. Aliases
4. Stdio

### Variables

Variable Providers are intended to give more control over what variables can be read or written.  The default implementation `HashMapVariableProvider` is the most basic implemenatation -
you have a name (a key) and that key might have a value.  However, more advanced implementations could be made, which could make variables read-only, require some form of privilege escalation,
hide the variable from reading without privilege escalation etc.

Though once using the shell, variables are handled via `set_variable` function, or via command `set`

There is a large difference from real shells, in that there is no concept of a "stack", all variables are "globals" (in the context of this shell - which you could have multiple shells and those vars are separate)
That also means there is no need to `export` a var, to make it available to the parent stack.

Examples:

```bash
$ set variablename variablevalue
$ echo $variablename
>    variablevalue
```

or

```bash
$ variablename=variablevalue
$ echo $variablename
>    variablevalue
```

### Commands

Similarly, command providers will accept different data on command registration - and then on execution will use that data to determine if the command should be allowed or any other
imaginitive logic you can think of. The default implementation `HashMapCommandProvider` has zero logic outside of what you'd expect for a hashmap. The key exists, or it doesn't.

You can of course, create more advanced implemenations.

Each command is given an execution context - which is the "environment" for the command.
It contains

1. Env: A snapshot of variables and command line arguments
2. ShellCtx: Kstdio, Shell, State

Clap was built-in to be as easy as possible to integrate.

Example clap command:

```rust
// Add clap::Parser derive
#[derive(clap::Parser)]
#[clap(name = "echo", about = "echo something to console")]
pub struct ExampleCmd {
    args: Vec<String>,
}

// And implement CommandExecutor for it
impl<CP: CommandProvider> CommandExecutor<CP> for ExampleCmd {
    fn execute(ctx: &mut ShellCtx<CP>, argv: &[String]) -> Result<(), KushError> {
        let args = Self::try_parse_from(argv)?;
        ctx.stdio.println()

        ctx.stdio.println(format!("Var $VARNAME: {}", env.variables.get("VARNAME")))?;

        Ok(())
    }
}

// Lets you just register it
fn register_cmds(ctx: &mut ShellCtx<CP>) {
    let c = ExampleCmd::build_clap_command();
    ctx.shell.register_command(c.get_name().to_owned(), _, Box::new(c))
}
```

## Shell expansion

When a command line is parsed, the command line is parsed into a CommandPipeline, which is a set of

1. Argv
2. OutputBehavior (PIPE - pass output to input of next | EXIT - in this case "EXIT" means, let the output flow to the original stdout)

When each command in the pipeline is executed, the argv are evaluated against the current variables.  This allows you to use string interpolation-like things:

```bash
$ set variable variablevalue
$ echo "The variable is: '$variable'"
>    The variable is 'variablevalue'

$ set variablename variable
$ echo "The variable with name '$variablename' is: '$variable'"
>    The variable with name 'variable' is 'variablevalue'
```

Or, you can use this to build dynamic variable names

```bash
$ set $variablename newvalue
$ echo "The variable with name '$variablename' is: '$variable'"
>    The variable with name 'variable' is 'newvalue'
```

## KStdio

I wanted to create an in-app shell, that let you have commands, aliases, variables - what's good in all of that - if you can't also pipe the output from one to another's input...

You can do this:

```bash
$ set variable1 one
$ set variable2 two
$ set var3 three

$ env
>    variable1: one
>    variable2: two
>    var3: three

$ env | grep variable
>    variable1: one
>    variable2: two
```

If you are using standard stdio - then it's easy...

```rust
let mut ctx = ShellCtx {
    stdio: KStdio::new_stdio()
    shell,
    state
};
Kush::execute(&mut ctx, "echo \"Hello\nagain\" | grep Hello").unwrap();


>     Hello
```

However, if you need the output through some other channel so you can provide your own in-app "Console", it's a little more involved obviously.

I really wanted to make this as simple as `new_channel` but I had problems doing that...  This way too - you can use whatever BufReader and Writer you want.
An implementation already exists  for `mpsc::channel` - `ReadReceiver` and `WriteSender`

```rust
// Make the channel
 let (tx, rx) = mpsc::channel(1024);
let tx = WriteSender::from(tx)
let tx = Box::new(tx) as Box<dyn Write>;

let mut ctx = ShellCtx {
    stdio: KStdio::new_stdio()KStdio::new(None, tx),
    shell,
    state
};

// Some magic function that reads Strings (lines) from rx and renders it into a console in the app.
let join_handle = spawn(relay_stdout(rx));

Kush::execute(&mut ctx, "echo \"Hello\nagain\" | grep Hello").unwrap();
//close the tx
drop(stdio);

// Let it consume then exit when it reaches the end due to drop
join(join_handle);
```

### Thoughts

#### m0re Haggard Parsing

I originally intended this to match bash, with as much attention as my condition allows me to give that... best attempt, not great, but the goal.
however, quake shell has some gnarly requiresments, such as `alias` commands not evaluating the command line... Vs using escaping *when you definie the alias... like normal...

So adding parsing to commandprovider might offer some customization here, bringing kushrs back to "best bash adhd can afford" - while still allowing kronik to override the parsing to handle parsing slightly differently based on the command name.
maybe there is another form of `alias` etc. Or a flag that can be ran that changes shell options to change how the command line is interpreted.

```rust
impl CommandProvider {
    // This would be useful for custom lang
    fn parse_line(line: &str) -> Pipeline

    // This would be useful for custom behavior in evaluation
    // Effectively the first half of Kush::execute_pipeline_command
    fn eval_pipeline_command(
        ctx: &mut ShellCtx<CP>,
        pipeline_cmd: PipelineCommand,
        parent_argv: &[Argv],
    ) -> Result<Vec<Argv>, KushError>;
}
```

#### Allow "Stacks"

I removed "Env" - which was to create a copy of vars because I couldn't support re-entry at the time. Now that I can, I removed it and to support "Temporary" values I added "ShadowVariableProvider" which can/is reset before/after every execution, dropping temp vars.
This is misleading though.  You have to use "set_shadow" to create a temporary var that will be dropped. When you set_var - it is still set using normal mechanisms in the inner vp.

Instead, consider supporting a stack of variables. And one must `export` a variable to push it up the stack. When the stack pops, the original variable values are returned to normal.

Some plumbing necessary in Kush
eg)

```rust
variable_provider.push_stack();
populate_argv_vars(variable_provider);
execute();
variable_provider.pop_stack();
```

and in `StackingVariableProvider`

```rust
impl StackingVariableProvider {
    fn push_stack(){
        // a copy of current stack
        let new_stack = self.stack.last().clone();
        self.stacks.push(new_stack);
    }
    fn pop_stack() {
        if self.stacks.len() > 1 {
            self.stacks.pop();
        }
    }
    fn export(name: &str) {
        if let Some(prev) = self.stacks.rev().skip(1).first() {
            prev.insert(name,v);
        }

    }
}
```

This is fairly drastic of a change for kronik though so not likely
