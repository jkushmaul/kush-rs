default:
  interruptible: true

stages:
  - build
  - test

workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

variables:
  FF_USE_FASTZIP: "true" # enable fastzip - a faster zip implementation that also supports level configuration.
  ARTIFACT_COMPRESSION_LEVEL: fastest # can also be set to fastest, fast, slow and slowest. If just enabling fastzip is not enough try setting this to fastest or fast.
  CACHE_COMPRESSION_LEVEL: fastest # same as above, but for caches
  TRANSFER_METER_FREQUENCY: 5s # will display transfer progress every 5 seconds for artifacts and remote caches.
  GIT_STRATEGY: clone
  GIT_DEPTH: 1
  CACHE_FALLBACK_KEY: ${CI_DEFAULT_BRANCH}-rust-${PLATFORM}

.linux-template:
  image: registry.gitlab.com/jkushmaul/rust_docker/rust-nightly-linux:latest
  variables:
    PLATFORM: linux
  tags:
    - linux-docker

.windows-template:
  image: registry.gitlab.com/jkushmaul/rust_docker/rust-nightly-windows-msvc:latest
  variables:
    PLATFORM: windows
  tags:
    - windows

.rust-template:
  variables:
    CARGO_HOME: "${CI_PROJECT_DIR}/.cache/cargo"
    SCCACHE_DIR: "${CI_PROJECT_DIR}/.cache/sccache"
  cache:
    - key: "${CI_COMMIT_REF_SLUG}-rust-${PLATFORM}"
      policy: pull
      paths:
        - "target/"
        - ".cache/"

.rust-linux:template:
  extends: [.linux-template,.rust-template]
  before_script:
    - rustup show
    - rustup --version
    - rustc --version
    - cargo --version
  #variables:
  #  RUSTC_WRAPPER: "/usr/bin/sccache"

.rust-windows:template:
  extends: [.windows-template,.rust-template]
  before_script:
    - echo "Image updated on $UPDATED_DATE"
    - rustup show
    - rustup --version
    - rustc --version
    - cargo --version

  #variables:
  #  RUSTC_WRAPPER: "C:\\cargo\\bin\\sccache.exe"

check:linux:
  extends: .rust-linux:template
  stage: build
  script:
    - cargo build
    - cargo doc
  cache:
    - key: "${CI_COMMIT_REF_SLUG}-rust-${PLATFORM}"
      policy: pull-push
      paths:
        - "target/"
        - ".cache/"

check:windows:
  extends: .rust-windows:template
  stage: build
  script:
    - cargo build
    - cargo doc
  cache:
    - key: "${CI_COMMIT_REF_SLUG}-rust-${PLATFORM}"
      policy: pull-push
      paths:
        - "target/"
        - ".cache/"

test:linux:
  extends: .rust-linux:template
  stage: test
  needs:
    - job: check:linux
      artifacts: false
  script:
    - rm -rf target/nextest target/coverage target/profraw || true
    - cargo nextest run
    - mkdir -p target/coverage
    - grcov target/profraw -s . --binary-path target/debug/deps --ignore src/tests -t html,lcov,cobertura --branch --ignore-not-existing --excl-br-line '^\s*((debug_)?assert(_eq|_ne)?!|#\[derive\()' --excl-line '^([ ]*}|)$' --excl-start '#\[cfg\(test\)\]' -o target/coverage
    - 'xmllint --xpath "concat(\"Coverage: \", 100 * string(//coverage/@line-rate), \"%\")" target/coverage/cobertura.xml'
  coverage: '/Coverage: \d+(?:\.\d+)?/'
  artifacts:
    paths:
      - target/coverage/
      - target/nextest
    reports:
      junit: target/nextest/default/junit.xml
      coverage_report:
        coverage_format: cobertura
        path:  target/coverage/cobertura.xml

test:windows:
  extends: [".rust-windows:template"]
  stage: test
  needs:
    - job: check:windows
      artifacts: false
  script:
    - if (Test-Path target/coverage) { Remove-Item -force -recurse target/coverage }
    - if (Test-Path target/profraw) { Remove-Item -force -recurse target/profraw }
    - if (Test-Path target/nextest) { Remove-Item -force -recurse target/nextest }
    - cargo nextest run
    - mkdir target/coverage
    - grcov target/profraw -s . --binary-path target/debug/deps -t html,lcov,cobertura --branch --ignore-not-existing --ignore '**/tests/*' --excl-br-line '^\s*((debug_)?assert(_eq|_ne)?!|#\[derive\()' --excl-line '^([ ]*}|)$' --excl-start '#\[cfg\(test\)\]' -o target/coverage
    #- 'xmllint --xpath "concat(\"Coverage: \", 100 * string(//coverage/@line-rate), \"%\")" target/coverage/cobertura.xml'
    - Select-Xml -Path ".\target\coverage\cobertura.xml" -XPath '//coverage/@line-rate'  | Select-Object -ExpandProperty Node | ForEach-Object { ([string](([float] $_.Value ) * 100.0 )) +"%" }
  coverage: '/Coverage: \d+(?:\.\d+)?/'
  artifacts:
    paths:
      - target/coverage/
      - target/nextest
    reports:
      junit: target/nextest/default/junit.xml
      coverage_report:
        coverage_format: cobertura
        path:  target/coverage/cobertura.xml
