/*
use super::CommandBehavior;


#[derive(Clone, Debug, PartialEq)]

pub struct TokenizedCommand {
    // cmd  arg1  arg2 -> Sequence(Literal("cmd"), Literal("arg1"), Literal("arg2"))
    // cmd  $1  $* -> Sequence(Literal("cmd"), Variable(Variable::Named("1")), Variable(Variable::Argv(1, None)))
    argv: Token,
    operation: CommandBehavior,
}*/

#[derive(Clone, Default, Debug, PartialEq, Eq)]
pub enum Token {
    /// Equivelant to None
    #[default]
    Empty,
    /// Just characters
    Literal(String),
    /// nested set of parsetokens
    Sequence(Vec<Token>),
    /// A variable, with options
    Variable(Variable, VariableFlag),
}
impl Token {
    pub fn literal(s: &str) -> Self {
        Self::Literal(s.to_string())
    }
}

#[derive(Clone, Default, Debug, PartialEq, Eq)]
pub enum VariableFlag {
    /// Use default behavior for the type
    #[default]
    None,
    /// Do escape quotes
    Quoted,
    /// Do not escape quotes
    AsIs,
    /// Does not cause failure (default)
    Optional,
    /// Causes command failure if not empty
    Required,
}

/// They can be named, dynamic with an eval, or they can be ranged argv

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum Variable {
    /// A literal name not argv
    Named(String),

    /// Dynamic
    Eval(Vec<Token>),

    ArgvExact(usize),

    /// An argv, or range
    ArgvMulti(ArgvRange),
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct ArgvRange {
    start: usize,
    len: Option<usize>,
}
impl ArgvRange {
    pub fn start(arg: usize) -> ArgvRange {
        Self {
            start: arg,
            len: None,
        }
    }
    pub fn get_start(&self) -> usize {
        self.start
    }
    pub fn get_len(&self) -> Option<usize> {
        self.len
    }
    /*
    pub fn get_range(&self, argv: &[Argv]) -> Range<usize> {
        if self.start >= argv.len() {
            return self.start..self.start;
        }
        let end = match self.len {
            Some(l) => {
                let l = l + 1;
                l.min(argv.len())
            }
            None => argv.len(),
        };
        return self.start..end;
    }
    */
}
