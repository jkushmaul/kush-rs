use super::CommandBehavior;

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum Argv {
    Quoted(String),
    Unquoted(String),
}
impl Argv {
    pub fn unquoted(s: &str) -> Self {
        Self::Unquoted(s.to_string())
    }

    pub fn quoted(s: &str) -> Self {
        Self::Quoted(s.to_string())
    }
    pub fn value(&self) -> &str {
        match self {
            Argv::Quoted(s) => s,
            Argv::Unquoted(s) => s,
        }
    }

    pub fn into_inner(self) -> String {
        match self {
            Argv::Quoted(s) => s,
            Argv::Unquoted(s) => s,
        }
    }

    pub fn is_quoted(&self) -> bool {
        matches!(self, Self::Quoted(_))
    }
}

/// Merely a more specific "Split" of argv's and the operation for each of them (pipe or not)
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct ParsedCmd {
    /// Each argv of this command operation
    pub argv: Vec<Argv>,
    /// The operation for argv (to pipe, or not to pipe)
    pub op: CommandBehavior,
}

impl ParsedCmd {
    /// Construct a new parsed argv
    pub fn new(argv: Vec<Argv>, op: CommandBehavior) -> Self {
        Self { argv, op }
    }
}

impl From<Vec<Argv>> for ParsedCmd {
    fn from(argv: Vec<Argv>) -> Self {
        Self {
            argv,
            op: CommandBehavior::ExitOutput,
        }
    }
}
