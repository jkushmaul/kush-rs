pub mod argv;
pub mod tokenized;

#[derive(Clone, Copy, Default, Debug, PartialEq, Eq)]

pub enum CommandBehavior {
    PipeOutput,
    #[default]
    ExitOutput,
}
