use std::fmt::Display;
use thiserror::Error;

#[derive(Debug, Error)]
pub enum KushParseError {
    #[error("Syntax Error: {0}")]
    Syntax(#[from] KushSyntaxError),
    #[error("General Error: {0}")]
    Other(String),
}

/// When parsing kush line, this will indicate what line, offset the error happened
#[derive(Error, Debug)]
pub struct KushSyntaxError {
    text: String,
    message: String,
    err_line: u32,
    offset: usize,
}

impl Display for KushSyntaxError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.get_help_string())
    }
}

impl KushSyntaxError {
    /// Constructs a new syntax error
    pub fn new(text: String, message: String, err_line: u32, offset: usize) -> Self {
        Self {
            text,
            message,
            err_line,
            offset,
        }
    }

    /// Getter for message
    pub fn get_message(&self) -> &str {
        &self.message
    }

    /// Getter for the text being parsed
    pub fn get_text(&self) -> &str {
        &self.text
    }

    /// Getter for the line the error occurred on
    pub fn get_line(&self) -> u32 {
        self.err_line
    }

    /// Getter for the offiset into text the error occurred on
    pub fn get_offset(&self) -> usize {
        self.offset
    }

    /// Helpful function to output a pointer to the line+offset of the error
    pub fn get_help_string(&self) -> String {
        let mut ptr = String::new();
        for _ in 0..self.offset {
            ptr.push('-');
        }
        ptr.push('^');
        format!("{}:\n{}\n{}", self.message, self.text, ptr)
    }
}
