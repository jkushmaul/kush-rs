//! Abstracted language structures for argv, and argv tokens, and implementations of parsers for each
mod nom_impl;

pub mod error;
pub mod parsed;

use error::KushParseError;
use parsed::{argv::ParsedCmd, tokenized::Token};

/// Used to pipe output to next command, or return output to root stdout

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum ParsedCommandOperation {
    /// Output will be returned to the root stdout
    ISOLATED,
    /// Output is piped into the next command
    PIPED,
}
/*
/// Used to represent within a single argv the various tokens that can exist there
#[derive(Clone, Debug, PartialEq)]
pub enum ParseToken {
    /// Or Literal ""
    Empty,
    /// Just characters
    Literal(String),
    /// nested set of parsetokens
    Sequence(Vec<ParseToken>),
    /// A variable, with options
    Variable(TokenVariable),
}

/// A tuple of ParseTokens, and a single command operation
#[derive(Clone, Debug, PartialEq)]
pub struct ParsedCommand {
    /// Each one is it's own argv.  There is one problem with this and that is positional argv with range. ${3-} or ${*} - those *expand into multiple argv...
    pub tokens: ParseToken,
    /// To pipe or not
    pub operation: ParsedCommandOperation,
}

/// Within a ParseToken, this represents a variable for evaluation
#[derive(Clone, Debug, PartialEq)]
pub struct TokenVariable {
    /// Some variables can be range-argv based, $*, or named
    pub name: VariableName,
    /// Used for evaluation
    pub modifier: Option<VariableModifier>,
}

/// They can be named, dynamic with an eval, or they can be ranged argv
#[derive(Clone, Debug, PartialEq)]
pub enum VariableName {
    /// A literal name not argv
    Named(String),
    /// Dynamic
    Eval(Vec<ParseToken>),
    /// An argv, or range
    Argv(VariablePosition),
}

/// Used with variable braces to determine how to evaluate
#[derive(Default, Copy, Clone, Debug, PartialEq)]
pub enum VariableModifier {
    /// Quotes will be escaped
    #[default]
    Quoted, //  q     - escape quotes
    /// Quotes will be output as is
    Asis, // asis  - do not escape quotes
    /// Default on eval to "" if not present
    Optional, // ?     - default to "" (What else would the result be...?)
    /// Fail if not present on eval
    Required, // !
}

/// Argv variables will have a number, or a range
#[derive(Copy, Clone, Debug, PartialEq)]
pub enum VariablePosition {
    /// A numeric
    Exact(usize),
    /// A range from a starting position to the end
    From(usize),
}
*/

/// Tokenize a string into an evaluation tree of variables and literals (and evals to make an additional tree)
/// Which now that I think of it - no tree is needed... it's just a sequence of literal and variables... Because there
/// are no control flow concepts...  So technically it could all ju st be a single sequence.  Just a slightly smarter "string builder"
pub fn parse_tokens(text: &str) -> Result<Token, KushParseError> {
    nom_impl::parse_tokens(text)
}

/// Parse a string into a set of command line strings (argv) and an operation (pipe, isolation)
/// Just a slightly smarter "split"
pub fn parse_argv_line(text: &str) -> Result<Vec<ParsedCmd>, KushParseError> {
    nom_impl::parse_argv_line(text)
}

#[cfg(test)]
mod tests {
    use super::nom_impl::parse_argv_line;
    use crate::parsed::{
        argv::{Argv, ParsedCmd},
        CommandBehavior,
    };

    #[test]
    fn test_parse_argv_line_multi_line() {
        let lines = "a little bit of this\na little bit of that\nor semi-colon; or | pipes";
        let parsed = parse_argv_line(lines).unwrap();

        assert_eq!(
            &parsed,
            &[
                ParsedCmd::new(
                    vec![
                        Argv::unquoted("a"),
                        Argv::unquoted("little"),
                        Argv::unquoted("bit"),
                        Argv::unquoted("of"),
                        Argv::unquoted("this")
                    ],
                    CommandBehavior::ExitOutput
                ),
                ParsedCmd::new(
                    vec![
                        Argv::unquoted("a"),
                        Argv::unquoted("little"),
                        Argv::unquoted("bit"),
                        Argv::unquoted("of"),
                        Argv::unquoted("that")
                    ],
                    CommandBehavior::ExitOutput
                ),
                ParsedCmd::new(
                    vec![Argv::unquoted("or"), Argv::unquoted("semi-colon")],
                    CommandBehavior::ExitOutput
                ),
                ParsedCmd::new(vec![Argv::unquoted("or")], CommandBehavior::PipeOutput),
                ParsedCmd::new(vec![Argv::unquoted("pipes")], CommandBehavior::ExitOutput)
            ]
        );
    }
}
