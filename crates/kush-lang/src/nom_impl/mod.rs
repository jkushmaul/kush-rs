mod argv;
mod tokens;

use crate::{
    error::KushSyntaxError,
    parsed::{argv::ParsedCmd, tokenized::Token},
    KushParseError,
};
use nom_locate::LocatedSpan;

type Span<'a> = LocatedSpan<&'a str>;

#[derive(Debug, PartialEq)]
struct KshellNomParserError<'a> {
    span: Span<'a>,
    message: String,
}
type IResult<'a, O> = nom::IResult<Span<'a>, O, KshellNomParserError<'a>>;

impl<'a> From<KshellNomParserError<'a>> for KushSyntaxError {
    fn from(e: KshellNomParserError<'a>) -> Self {
        let text = e.span.to_string();
        Self::new(text, e.message.to_owned(), e.line(), e.offset())
    }
}

impl<'a> KshellNomParserError<'a> {
    pub fn new(message: String, span: Span<'a>) -> Self {
        Self { span, message }
    }

    pub fn line(&self) -> u32 {
        self.span.location_line()
    }

    pub fn offset(&self) -> usize {
        self.span.location_offset()
    }
}

// That's what makes it nom-compatible.
impl<'a> nom::error::ParseError<Span<'a>> for KshellNomParserError<'a> {
    fn from_error_kind(input: Span<'a>, kind: nom::error::ErrorKind) -> Self {
        Self::new(format!("parse error {:?}", kind), input)
    }

    fn append(_input: Span<'a>, _kind: nom::error::ErrorKind, other: Self) -> Self {
        other
    }

    fn from_char(input: Span<'a>, c: char) -> Self {
        Self::new(format!("unexpected character '{}'", c), input)
    }
}

pub fn parse_tokens(text: &str) -> Result<Token, KushParseError> {
    tokens::parse_tokens(text)
}

pub fn parse_argv_line(text: &str) -> Result<Vec<ParsedCmd>, KushParseError> {
    argv::parse_argv_line(text)
}
