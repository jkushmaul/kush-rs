use super::{IResult, KshellNomParserError, Span};
use crate::{
    error::KushSyntaxError,
    parsed::tokenized::{ArgvRange, Token, Variable, VariableFlag},
    KushParseError,
};
use nom::{
    branch::alt,
    bytes::complete::tag,
    //The language should make naming things reserved words illegal. Shot on site.  One person names it the way they want.
    //150m other people have to make an alias for it... it doesn't make sense.
    character::complete::{alphanumeric1, digit1, multispace1, none_of},
    combinator::{eof, opt, recognize},
    multi::{many0, many1, many_till},
    sequence::preceded,
};
use nom_locate::position;

pub fn parse_tokens(text: &str) -> Result<Token, KushParseError> {
    let s = Span::new(text);
    let result = match many_till(alt((dollar_literal, token_variable, token_literal)), eof)(s) {
        Ok((_, (result, _))) => result,
        Err(nom::Err::Error(e)) => return Err(KushSyntaxError::from(e))?,
        Err(e) => return Err(KushParseError::Other(format!("Error in parser: {}", e))),
    };
    if result.is_empty() {
        return Ok(Token::Empty);
    }
    if result.len() == 1 {
        let mut result = result;
        return Ok(result.remove(0));
    }
    let result = Token::Sequence(result);
    Ok(result)
}

fn variable_type_argv_all(s: Span) -> IResult<Variable> {
    let (s, _) = tag("*")(s)?;
    let argv_range = ArgvRange::start(1);

    Ok((s, Variable::ArgvMulti(argv_range)))
}

fn variable_type_argv_from(s: Span) -> IResult<Variable> {
    let (s, var) = recognize(digit1)(s)?;

    let pos: usize = match var.parse() {
        Ok(pos) => pos,
        Err(e) => {
            return Err(nom::Err::Error(KshellNomParserError {
                span: s,
                message: format!("Integer parse error {e}"),
            }));
        }
    };

    //There may be a `-` to indicate all after
    let (s, r) = opt(tag("-"))(s)?;
    let pos = match r {
        Some(_) => {
            let argv_range = ArgvRange::start(pos);
            Variable::ArgvMulti(argv_range)
        }
        None => Variable::ArgvExact(pos),
    };

    Ok((s, pos))
}

fn variable_type_argv(s: Span) -> IResult<Variable> {
    let (s, var) = alt((variable_type_argv_all, variable_type_argv_from))(s)?;

    Ok((s, var))
}

fn variable_type_named(s: Span) -> IResult<Variable> {
    let (s, var) = recognize(many1(alt((alphanumeric1, tag("_")))))(s)?;
    Ok((s, Variable::Named(var.to_string())))
}

fn variable_modifier_optional(s: Span) -> IResult<VariableFlag> {
    let (s, _) = tag("?")(s)?;
    Ok((s, VariableFlag::Optional))
}

fn variable_modifier_asis(s: Span) -> IResult<VariableFlag> {
    let (s, _) = tag("asis")(s)?;
    Ok((s, VariableFlag::AsIs))
}

fn variable_modifier_required(s: Span) -> IResult<VariableFlag> {
    let (s, _) = tag("!")(s)?;
    Ok((s, VariableFlag::Required))
}

fn variable_modifier_quoted(s: Span) -> IResult<VariableFlag> {
    let (s, _) = tag("q")(s)?;
    Ok((s, VariableFlag::Quoted))
}
fn variable_modifier(s: Span) -> IResult<VariableFlag> {
    let (s, modifier) = alt((
        variable_modifier_optional,
        variable_modifier_asis,
        variable_modifier_quoted,
        variable_modifier_required,
    ))(s)?;

    Ok((s, modifier))
}

fn variable_name_std(s: Span) -> IResult<Variable> {
    let (s, var) = alt((variable_type_argv, variable_type_named))(s)?;

    Ok((s, var))
}
fn variable_name_dynamic(s: Span) -> IResult<Variable> {
    let (s, v) = token_variable(s)?;
    Ok((s, Variable::Eval(vec![v])))
}
fn variable_name_braces(s: Span) -> IResult<Variable> {
    let (s, var) = alt((
        variable_name_dynamic,
        variable_type_argv,
        variable_type_named,
    ))(s)?;

    Ok((s, var))
}

fn variable_braces(s: Span) -> IResult<Token> {
    let (s, _) = tag("{")(s)?;
    let (s, _) = position(s)?;
    let (s, var) = variable_name_braces(s)?;
    // Exception: $* and $n- don't use the quoted form by default (That'd be "asis" for $* or ${N-} because they are "Multiple")
    let (s, modifier) = opt(preceded(multispace1, variable_modifier))(s)?;
    let (s, _) = tag("}")(s)?;

    let flag = modifier.unwrap_or_default();
    Ok((s, Token::Variable(var, flag)))
}

fn variable_std(s: Span) -> IResult<Token> {
    // Default variable modifier
    let (s, _) = position(s)?;
    let (s, name) = variable_name_std(s)?;
    let modifier = VariableFlag::default();
    Ok((s, Token::Variable(name, modifier)))
}

fn token_variable(s: Span) -> IResult<Token> {
    let (s, _) = tag("$")(s)?;
    let (s, _) = position(s)?;
    alt((variable_braces, variable_std))(s)
}

fn token_literal(s: Span) -> IResult<Token> {
    let (s, _) = position(s)?;
    let (s, literal) = recognize(many0(none_of("$")))(s)?;

    Ok((s, Token::Literal(literal.to_string())))
}

fn dollar_literal(s: Span) -> IResult<Token> {
    let (s, _) = tag("$")(s)?;
    let (s, _) = tag("$")(s)?;

    Ok((s, Token::Literal("$".to_string())))
}

#[cfg(test)]
mod tests {
    use crate::{
        error::KushSyntaxError,
        nom_impl::tokens::parse_tokens,
        parsed::tokenized::{ArgvRange, Token, Variable, VariableFlag},
    };

    use super::{variable_braces, variable_std, Span};

    #[test]
    fn test_parse_tokens_quoted_eval() {
        let input = "\"${$1 ?}\"";

        let eval = Token::Variable(
            Variable::Eval(vec![Token::Variable(
                Variable::ArgvExact(1),
                VariableFlag::None,
            )]),
            VariableFlag::Optional,
        );
        let dbl_quote = Token::Literal("\"".to_string());
        let expected = Token::Sequence(vec![dbl_quote.clone(), eval, dbl_quote]);

        let actual = parse_tokens(input).unwrap();
        assert_eq!(actual, expected);
    }

    #[test]
    fn test_multiple_all_default() {
        let cmd = "cmd $*";
        let actual = super::parse_tokens(cmd).unwrap();

        let expected = Token::Sequence(vec![
            Token::Literal("cmd ".into()),
            Token::Variable(
                Variable::ArgvMulti(ArgvRange::start(1)),
                VariableFlag::default(),
            ),
        ]);
        assert_eq!(actual, expected);
    }

    #[test]
    fn test_multiple_required() {
        let cmd = "literal ${1 !}";
        let actual = super::parse_tokens(cmd).unwrap();

        let expected = Token::Sequence(vec![
            Token::Literal("literal ".to_string()),
            Token::Variable(Variable::ArgvExact(1), VariableFlag::Required),
        ]);

        assert_eq!(actual, expected);
    }

    #[test]
    fn test_nested_variable_braces() {
        let cmd = "{$1 ?}"; // The preceding $ is already identified
        let (_, actual) = match variable_braces(Span::new(cmd)) {
            Ok(a) => a,
            Err(nom::Err::Error(e)) => panic!("Failed to parse: {}", KushSyntaxError::from(e)),
            Err(e) => panic!("Failed to parse: {}", e),
        };
        let dynamic_name = Token::Variable(Variable::ArgvExact(1), VariableFlag::default());
        let expected = Token::Variable(Variable::Eval(vec![dynamic_name]), VariableFlag::Optional);

        assert_eq!(actual, expected);
    }

    #[test]
    fn test_token_literal() {
        let input = Span::new("asimple literal");

        let (next, actual) = super::token_literal(input).unwrap();
        assert_eq!(next.len(), 0);
        let expected = Token::Literal("asimple literal".to_owned());

        assert_eq!(actual, expected);
    }

    #[test]
    fn test_variable_braces() {
        let input = Span::new("{var1_name}");

        let (next, actual) = variable_braces(input).unwrap();
        assert_eq!(next.len(), 0);
        let expected = Token::Variable(
            Variable::Named("var1_name".to_owned()),
            VariableFlag::default(),
        );

        assert_eq!(actual, expected);
    }
    #[test]
    fn test_variable_std() {
        let input = Span::new("var1_name");

        let (next, actual) = variable_std(input).unwrap();
        assert_eq!(next.len(), 0);
        let expected = Token::Variable(
            Variable::Named("var1_name".to_owned()),
            VariableFlag::default(),
        );

        assert_eq!(actual, expected);
    }
    #[test]
    fn test_token_variable_std() {
        let input = Span::new("$var1_name");

        let (next, actual) = super::token_variable(input).unwrap();
        assert_eq!(next.len(), 0);

        let expected = Token::Variable(
            Variable::Named("var1_name".to_owned()),
            VariableFlag::default(),
        );
        assert_eq!(actual, expected);

        let input = Span::new("${var1_name}");
        let (next, actual) = super::token_variable(input).unwrap();

        assert_eq!(next.len(), 0);

        let expected = Token::Variable(
            Variable::Named("var1_name".to_owned()),
            VariableFlag::default(),
        );

        assert_eq!(actual, expected);
    }
    #[test]
    fn test_token_variable_braces() {
        let input = Span::new("${var1_name}");
        let (next, actual) = super::token_variable(input).unwrap();

        assert_eq!(next.len(), 0);

        let expected = Token::Variable(
            Variable::Named("var1_name".to_owned()),
            VariableFlag::default(),
        );

        assert_eq!(actual, expected);
    }

    // These have to be split out into token tests.
    #[test]
    fn test_tokens_kitchen_sink() {
        let input = "mixed \"${argv1_name} and literal\";        and many spaces";

        let actual = super::parse_tokens(input).unwrap();
        let var = Token::Variable(
            Variable::Named("argv1_name".to_owned()),
            VariableFlag::default(),
        );

        let expected = Token::Sequence(vec![
            Token::Literal("mixed \"".to_owned()),
            var,
            Token::Literal(" and literal\";        and many spaces".to_owned()),
        ]);

        assert_eq!(actual, expected);
    }
}
