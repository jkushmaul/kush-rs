//! A first tage parsing of the command line
use crate::{
    error::{KushParseError, KushSyntaxError},
    parsed::{
        argv::{Argv, ParsedCmd},
        CommandBehavior,
    },
};

use super::{IResult, Span};
use nom::{
    branch::alt,
    bytes::complete::tag,
    //The language should make naming things reserved words illegal. Shot on site.  One person names it the way they want.
    //150m other people have to make an alias for it... it doesn't make sense.
    character::complete::{alphanumeric1, none_of, space1},
    combinator::{eof, opt, peek, recognize, value},
    multi::{many0, many1, many_till},
    sequence::delimited,
};
use nom_locate::{position, LocatedSpan};

pub fn parse_argv_line(text: &str) -> Result<Vec<ParsedCmd>, KushParseError> {
    let s = Span::new(text);

    let (s, result) = match many_till(argv_cmd, alt((argv_line_comment, eof)))(s) {
        Ok((s, (result, _))) => (s, result),
        Err(nom::Err::Error(e)) => return Err(KushSyntaxError::from(e))?,
        Err(e) => return Err(KushParseError::Other(format!("Error in parser: {e}"))),
    };

    if s.is_empty() {
        return Ok(result);
    }
    Err(KushParseError::Syntax(KushSyntaxError::new(
        text.into(),
        format!("Trailing characters at offset {}", s.len()),
        s.location_line(),
        s.location_offset(),
    )))
}

fn match_cmd_operation_pipe(s: Span) -> IResult<CommandBehavior> {
    let (s, _) = position(s)?;
    return tag("|")(s).map(|(a, _)| (a, CommandBehavior::PipeOutput));
}
fn match_cmd_operation_isolated(s: Span) -> IResult<CommandBehavior> {
    let (s, _) = position(s)?;
    return alt((
        tag(";"),
        argv_line_comment,
        tag("\r\n"),
        tag("\n"),
        peek(eof),
    ))(s)
    .map(|(a, _)| (a, CommandBehavior::ExitOutput));
}

fn parse_cmd_operation(s: Span) -> IResult<CommandBehavior> {
    let (s, _) = position(s)?;
    let (s, _) = opt(space1)(s)?;

    let (s, r) = alt((match_cmd_operation_isolated, match_cmd_operation_pipe))(s)?;

    Ok((s, r))
}

/*
fn escaped_backslash(s: Span) -> IResult<&str> {
    let (s, r) = nom::combinator::recognize(nom::character::complete::char('\\'))(s)?;
    Ok((s, &r))
}

fn escaped_quote(s: Span) -> IResult<&str> {
    let (s, r) = nom::combinator::recognize(nom::character::complete::char('\"'))(s)?;
    Ok((s, &r))
}

fn transform_quotes_escaped_bad(s: Span) -> IResult<std::string::String> {
    let (s, r) = nom::bytes::complete::escaped_transform(
        nom::bytes::complete::is_not("\\"),
        '\\',
        //alt((escaped_backslash, escaped_quote)),
        alt((
            value("\\", tag("\\")),
            value(" ", tag(" ")),
            value("\t", tag("t")),
            value("\"", tag("\"")),
            value("\n", tag("n")),
        )),
    )(s)?;

    Ok((s, r))
}

fn transform_literal_escaped(s: Span) -> IResult<std::string::String> {
    let (s, r) = nom::bytes::complete::escaped_transform(
        nom::bytes::complete::is_not(" "),
        '\\',
        alt((escaped_backslash, escaped_quote)),
    )(s)?;

    Ok((s, r))
}
*/
fn transform_quotes_escaped(s: Span) -> IResult<std::string::String> {
    let (s, r) = nom::bytes::complete::escaped_transform(
        nom::bytes::complete::is_not("\\\""),
        '\\',
        //alt((escaped_quote, escaped_backslash)),
        alt((
            value("\\", tag("\\")),
            value(" ", tag(" ")),
            value("\t", tag("t")),
            value("\"", tag("\"")),
            value("\n", tag("n")),
        )),
    )(s)?;

    Ok((s, r))
}

/// Everything up to the closing quote, mind the escaped characters
/// "arg0 arg1"      -> "arg0 arg1"
/// "arg0 \"arg1\""  -> "arg0 \"arg1\""
/// "arg0"; arg1     -> "arg0"
fn argv_quoted(s: Span) -> IResult<Argv> {
    let (s, r) = delimited(tag("\""), opt(transform_quotes_escaped), tag("\""))(s)?;

    let r = r.unwrap_or_default();
    Ok((s, Argv::Quoted(r)))
}

fn argv_variable_braces(s: Span) -> IResult<String> {
    //special handling if
    let (s, _) = tag("{")(s)?;
    let (s, r) = recognize(many0(none_of("}")))(s)?;
    let (s, _) = tag("}")(s)?;

    Ok((s, format!("{{{}}}", &r)))
}
fn argv_variable_std(s: Span) -> IResult<String> {
    let (s, var) = alt((tag("*"), recognize(many1(alt((alphanumeric1, tag("_")))))))(s)?;
    let var = var.to_string();
    Ok((s, var))
}

fn argv_variable_dollar(s: Span) -> IResult<String> {
    let (s, var) = tag("$")(s)?;
    let var = var.to_string();
    Ok((s, var))
}
fn argv_variable(s: Span) -> IResult<String> {
    let (s, _) = tag("$")(s)?;
    let (s, r) = alt((
        argv_variable_dollar,
        argv_variable_braces,
        argv_variable_std,
    ))(s)?;
    let r = format!("${}", r);
    Ok((s, r))
}

fn argv_literal(s: Span) -> IResult<String> {
    let (s, r) = recognize(many1(none_of("\n$\t ;|\"")))(s)?;
    let r = r.to_string();
    Ok((s, r))
}

/// Anything up to the first space.  Mind the escaped characters
/// arg0 arg1   -> "arg0"
/// arg\ 0      -> "arg 0"
/// arg0;arg1   -> "arg0"
fn argv_unquoted(s: Span) -> IResult<Argv> {
    let (s, r) = many1(alt((argv_variable, argv_literal)))(s)?;

    let r = r.join("");

    Ok((s, Argv::Unquoted(r)))
}

/// A quoted or an unquoted
fn argv(s: Span) -> IResult<Argv> {
    let (s, _) = opt(space1)(s)?;
    let (s, r) = match argv_quoted(s) {
        Ok(v) => v,
        Err(_) => argv_unquoted(s)?,
    };

    Ok((s, r))
}

/// This should allow multiple spaces to wrap each argv which are either literals or double quotes
/// followed by either |, ;, or end of file (which defaults to ;)
fn argv_cmd(s: Span) -> IResult<ParsedCmd> {
    let (s, _) = opt(space1)(s)?;

    let (s, (argv, op)) = many_till(
        delimited(opt(space1), argv, opt(space1)),
        parse_cmd_operation,
    )(s)?;

    Ok((s, ParsedCmd { argv, op }))
}

fn argv_line_comment(s: Span) -> IResult<LocatedSpan<&str>> {
    let (s, _) = position(s)?;
    let (s, _) = tag("//")(s)?;

    let (s, c) = recognize(many0(none_of("\n")))(s)?;

    Ok((s, c))
}

#[cfg(test)]
mod tests {
    use crate::{
        error::KushSyntaxError,
        nom_impl::argv::{argv, ParsedCmd, Span},
        parsed::{argv::Argv, CommandBehavior},
    };

    #[test]
    fn test_parse_argv_newline() {
        let lines = "new\nline";

        let (remainder, actual) = argv(Span::new(lines)).unwrap();
        assert_eq!(actual, Argv::unquoted("new"));
        assert_eq!(&remainder.to_string(), "\nline");
    }

    #[test]
    fn test_problematic_escaped_dollar() {
        let s = "sv_cmd $$*";
        let actual = super::parse_argv_line(s).unwrap();
        let expected = vec![ParsedCmd {
            argv: vec![Argv::unquoted("sv_cmd"), Argv::unquoted("$$*")],
            op: CommandBehavior::ExitOutput,
        }];
        assert_eq!(actual, expected);
    }

    #[test]
    fn test_spaced_argv() {
        let s = Span::new(" argv");
        let (_, actual) = super::argv(s).unwrap();
        assert_eq!(actual, Argv::unquoted("argv"));
    }
    #[test]
    fn test_argv_unquoted() {
        let s = Span::new("_forced_saved_cvar_${1 ?}");
        let (_, actual) = super::argv_unquoted(s).unwrap();
        assert_eq!(actual, Argv::unquoted("_forced_saved_cvar_${1 ?}"));
    }

    #[test]
    fn test_problematic_multi_cmd4() {
        // alias _menu_forced_saved_cvars_next ""
        let alias_cmd = Argv::quoted("set _forced_saved_cvar_ doit; set _forced_saved_cvar_${1 ?} done; _menu_forced_saved_cvars_chck ${* ?}");
        let s = format!(
            "alias _menu_forced_saved_cvars_next \"{}\"",
            alias_cmd.value()
        );
        let actual = super::parse_argv_line(&s).unwrap();
        let expected = vec![ParsedCmd {
            argv: vec![
                Argv::unquoted("alias"),
                Argv::unquoted("_menu_forced_saved_cvars_next"),
                alias_cmd.clone(),
            ],
            op: CommandBehavior::ExitOutput,
        }];
        assert_eq!(actual, expected);

        let actual = super::parse_argv_line(alias_cmd.value()).unwrap();
        let expected = vec![
            ParsedCmd {
                argv: vec![
                    Argv::unquoted("set"),
                    Argv::unquoted("_forced_saved_cvar_"),
                    Argv::unquoted("doit"),
                ],
                op: CommandBehavior::ExitOutput,
            },
            ParsedCmd {
                argv: vec![
                    Argv::unquoted("set"),
                    Argv::unquoted("_forced_saved_cvar_${1 ?}"),
                    Argv::unquoted("done"),
                ],
                op: CommandBehavior::ExitOutput,
            },
            ParsedCmd {
                argv: vec![
                    Argv::unquoted("_menu_forced_saved_cvars_chck"),
                    Argv::unquoted("${* ?}"),
                ],
                op: CommandBehavior::ExitOutput,
            },
        ];
        assert_eq!(actual, expected);
    }

    #[test]
    fn test_argv_unclosed_quote() {
        let s = "\"quoted should have an ending";
        if let Ok((s, result)) = super::argv(Span::new(s)) {
            panic!(
                "Did not expect to see a result: s: {:?}, result: {:?}",
                s, result
            );
        }
    }

    #[test]
    fn test_parse_argv_line_unclosed_quote() {
        let s = "this \"quoted should have an ending";
        if let Ok(s) = super::parse_argv_line(s) {
            panic!("Did not expect to see a result: {:?}", s);
        }
    }

    #[test]
    fn test_first_quoted() {
        let s = "\"alias\" rec \"record demos/${1 !}\"";
        let actual = super::parse_argv_line(s).unwrap();
        let expected = vec![ParsedCmd {
            argv: vec![
                Argv::quoted("alias"),
                Argv::unquoted("rec"),
                Argv::quoted("record demos/${1 !}"),
            ],
            op: CommandBehavior::ExitOutput,
        }];
        assert_eq!(actual, expected);
    }

    #[test]
    fn test_problematic_alias_required_arg() {
        let s = "alias rec \"record demos/${1 !}\"";
        let actual = super::parse_argv_line(s).unwrap();
        let expected = vec![ParsedCmd {
            argv: vec![
                Argv::unquoted("alias"),
                Argv::unquoted("rec"),
                Argv::quoted("record demos/${1 !}"),
            ],
            op: CommandBehavior::ExitOutput,
        }];
        assert_eq!(actual, expected);
    }

    #[test]
    fn test_problematic_multi_cmd_alias3() {
        let s = "cvar_resettodefaults_saveonly; exec ${* !}";
        let actual = super::parse_argv_line(s).unwrap();
        let expected = vec![
            ParsedCmd {
                argv: vec![Argv::unquoted("cvar_resettodefaults_saveonly")],

                op: CommandBehavior::ExitOutput,
            },
            ParsedCmd {
                argv: vec![Argv::unquoted("exec"), Argv::unquoted("${* !}")],
                op: CommandBehavior::ExitOutput,
            },
        ];
        assert_eq!(actual, expected);
    }
    #[test]
    fn test_argv_variable_braces() {
        let cmd = "{argv ?}";

        let (_, actual) = super::argv_variable_braces(Span::new(cmd)).unwrap();

        assert_eq!(actual, cmd);
    }
    #[test]
    fn test_argv_variable_braces_star() {
        let cmd = "{* ?}";

        let (_, actual) = super::argv_variable_braces(Span::new(cmd)).unwrap();

        assert_eq!(actual, cmd);
    }

    #[test]
    fn test_argv_variable_std_star() {
        let cmd = "*";

        let (_, actual) = super::argv_variable_std(Span::new(cmd)).unwrap();

        assert_eq!(actual, cmd);
    }

    #[test]
    fn test_argv_var_with_braces_and_modifier() {
        let cmd = "cmd ${argv ?} \"${brokenvar ?\" \"${okbutquotedvar ?}\"";

        let actual = super::parse_argv_line(cmd).unwrap();
        let expected: Vec<_> = vec![
            Argv::unquoted("cmd"),
            Argv::unquoted("${argv ?}"),
            Argv::quoted("${brokenvar ?"),
            Argv::quoted("${okbutquotedvar ?}"),
        ];
        let expected = vec![ParsedCmd {
            argv: expected,
            op: CommandBehavior::ExitOutput,
        }];
        assert_eq!(actual, expected);
    }
    #[test]
    fn test_argv_dynamic_var() {
        let cmd = "cmd ${$1 ?}";

        let actual = super::parse_argv_line(cmd).unwrap();
        let expected = vec![ParsedCmd {
            argv: vec![Argv::unquoted("cmd"), Argv::unquoted("${$1 ?}")],
            op: CommandBehavior::ExitOutput,
        }];
        assert_eq!(actual, expected);
    }
    #[test]
    fn test_problematic_nested_vars() {
        let cmd = "seta $1 \"${$1 ?}\"";
        let alias = format!(
            "alias makesaved \"{}\"",
            cmd.replace("\\", "\\\\").replace("\"", "\\\"")
        );
        let actual = super::parse_argv_line(&alias).unwrap();
        let expected: Vec<_> = vec![
            Argv::unquoted("alias"),
            Argv::unquoted("makesaved"),
            Argv::quoted(cmd),
        ];
        let expected = vec![ParsedCmd {
            argv: expected,
            op: CommandBehavior::ExitOutput,
        }];
        assert_eq!(actual, expected);
    }

    #[test]
    fn test_transform_quotes_escaped_quote_2() {
        let s = Span::new("a \\\" quote and \" unescaped");
        let (_, actual) = super::transform_quotes_escaped(s).unwrap();
        assert_eq!(actual, "a \" quote and ");
    }

    #[test]
    fn test_transform_quotes_escaped_quote() {
        let s = Span::new("a \\\" quote");
        let (_, actual) = super::transform_quotes_escaped(s).unwrap();
        assert_eq!(actual, "a \" quote");
    }

    #[test]
    fn test_transform_quotes_escaped_backslash() {
        let s = Span::new("a \\\\ backslash");
        let (_, actual) = super::transform_quotes_escaped(s).unwrap();
        assert_eq!(actual, "a \\ backslash");
    }

    #[test]
    fn test_argv_quoted_empty() {
        let input = "\"\"";
        let s = Span::new(input);
        let (s, r) = super::argv_quoted(s).unwrap();
        assert_eq!(s.len(), 0);
        assert_eq!(r.value(), "");
    }
    #[test]
    fn test_argv_quoted_escaped() {
        let input = "\"set \\\"$1\\\"\"";
        let expected = Argv::quoted("set \"$1\"");
        //let (_, actual) = transform_quotes_escaped(Span::new(input)).unwrap();
        //assert_eq!(actual, expected);
        let actual = match super::argv_quoted(Span::new(input)) {
            Ok((_, r)) => r,
            Err(nom::Err::Error(e)) => panic!("Failed to parse: {}", KushSyntaxError::from(e)),
            Err(e) => panic!("Parse error: {}", e),
        };

        assert_eq!(actual, expected);
    }
    /*
    Executing cmd: alias setreport "set \"$1\" \"$2\" ; sendcvar \"$1\""
        thread 'main' panicked at crates\kbaseq3_vm\examples/csqc_init.rs:254:21:
        Error executing command xonotic-client.cfg#6:  Command error: Alias command must have 2 args
            -> 'alias setreport "set \"$1\" \"$2\" ; sendcvar \"$1\""'
     */
    #[test]
    fn test_problematic_multi_cmd_alias2() {
        let cmd = "set \"$1\" \"$2\" ; sendcvar \"$1\"";
        let input = format!(
            "alias setreport \"{}\"",
            cmd.replace("\\", "\\\\").replace("\"", "\\\"")
        );
        let actual = super::parse_argv_line(&input).unwrap();
        let expected = vec![ParsedCmd {
            argv: vec![
                Argv::unquoted("alias"),
                Argv::unquoted("setreport"),
                Argv::quoted(cmd),
            ],
            op: CommandBehavior::ExitOutput,
        }];

        assert_eq!(actual, expected);
    }

    #[test]
    fn test_problematic_multi_cmd_alias() {
        let input = "alias _menu_forced_saved_cvars_next\t\"set _forced_saved_cvar_ doit; \tset _forced_saved_cvar_${1 ?} done; _menu_forced_saved_cvars_chck ${* ?}\"";
        let actual = super::parse_argv_line(input).unwrap();
        let expected = vec![
            ParsedCmd {argv:
                vec![Argv::unquoted("alias"), Argv::unquoted("_menu_forced_saved_cvars_next"), Argv::quoted("set _forced_saved_cvar_ doit; \tset _forced_saved_cvar_${1 ?} done; _menu_forced_saved_cvars_chck ${* ?}") ],
                op: CommandBehavior::ExitOutput,
            },

        ];

        assert_eq!(actual, expected);
    }

    #[test]
    fn test_problematic_multi_cmd2() {
        let input = "seta g_configversion 0   \"Configuration file version (used to upgrade settings) 0: first run, or previous start was <2.4.1  Later, it's overridden by config.cfg, version ranges are defined in config_update.cfg\"";
        let actual = super::parse_argv_line(input).unwrap();
        let expected = vec![
            ParsedCmd {argv:
                vec![Argv::unquoted("seta"), Argv::unquoted("g_configversion"), Argv::unquoted("0"), Argv::quoted("Configuration file version (used to upgrade settings) 0: first run, or previous start was <2.4.1  Later, it's overridden by config.cfg, version ranges are defined in config_update.cfg")],
                op: CommandBehavior::ExitOutput,
            },

        ];

        assert_eq!(actual, expected);
    }

    #[test]
    fn test_problematic_multi_cmd() {
        let input = "seta \"userbind1_press\" \"say_team strength soon\";  seta \"userbind1_release\" \"\";  seta \"userbind1_description\" \"team: strength soon\"";
        let actual = super::parse_argv_line(input).unwrap();
        let expected = vec![
            ParsedCmd {
                argv: vec![
                    Argv::unquoted("seta"),
                    Argv::quoted("userbind1_press"),
                    Argv::quoted("say_team strength soon"),
                ],
                op: CommandBehavior::ExitOutput,
            },
            ParsedCmd::new(
                vec![
                    Argv::unquoted("seta"),
                    Argv::quoted("userbind1_release"),
                    Argv::quoted(""),
                ],
                CommandBehavior::ExitOutput,
            ),
            ParsedCmd::new(
                vec![
                    Argv::unquoted("seta"),
                    Argv::quoted("userbind1_description"),
                    Argv::quoted("team: strength soon"),
                ],
                CommandBehavior::ExitOutput,
            ),
        ];

        assert_eq!(actual, expected);
    }

    #[test]
    fn test_argv_quoted() {
        let input = "\"set $*\"";
        let s = Span::new(input);
        let (_, actual_arg) = super::argv_quoted(s).unwrap();
        let expected_arg = Argv::quoted("set $*");

        assert_eq!(actual_arg, expected_arg);
    }

    #[test]
    fn test_argv_literal() {
        let input = "alias";
        let s = Span::new(input);
        let (_, actual_args) = super::argv_literal(s).unwrap();
        let expected_args = "alias";

        assert_eq!(actual_args, expected_args);
    }
    /*
        #[test]
        fn test_argv_list() {
            let input = "alias seta \"set $*\"";
            let s = Span::new(input);
            let (_, actual_args) = argv_list(s).unwrap();
            let expected_args = vec!["alias", "seta", "set $*"];

            assert_eq!(actual_args, expected_args);
        }
    */
    #[test]
    fn test_argv_cmd() {
        let input = "alias seta \"set $*\"";
        let s = Span::new(input);
        let (_, actual) = super::argv_cmd(s).unwrap();
        let expected_args = vec![
            Argv::unquoted("alias"),
            Argv::unquoted("seta"),
            Argv::quoted("set $*"),
        ];
        let expected_op = CommandBehavior::ExitOutput;
        assert_eq!(actual.argv, expected_args);
        assert_eq!(actual.op, expected_op);
    }

    #[test]
    fn test_parse_line_argv() {
        let input = "alias seta \"set $*\"";
        let actual = super::parse_argv_line(input).unwrap();
        let expected = vec![ParsedCmd::new(
            vec![
                Argv::unquoted("alias"),
                Argv::unquoted("seta"),
                Argv::quoted("set $*"),
            ],
            CommandBehavior::ExitOutput,
        )];
        assert_eq!(actual, expected);
    }

    #[test]
    fn test_piped_parse_line_argv() {
        let input = "test_write | test_read";
        let actual = match super::parse_argv_line(input) {
            Ok(s) => s,
            Err(e) => panic!("Error parsing: {}", e),
        };
        let expected = vec![
            ParsedCmd::new(
                vec![Argv::unquoted("test_write")],
                CommandBehavior::PipeOutput,
            ),
            ParsedCmd::new(
                vec![Argv::unquoted("test_read")],
                CommandBehavior::ExitOutput,
            ),
        ];
        assert_eq!(actual, expected);
    }

    #[test]
    fn simple_multiple_command() {
        let input = "dothis arg0;then arg0 \"and arg1\";this|tothis; thenthat";
        let actual = match super::parse_argv_line(input) {
            Ok(s) => s,
            Err(e) => panic!("Error parsing: {}", e),
        };
        let expected = vec![
            ParsedCmd::new(
                vec![Argv::unquoted("dothis"), Argv::unquoted("arg0")],
                CommandBehavior::ExitOutput,
            ),
            ParsedCmd::new(
                vec![
                    Argv::unquoted("then"),
                    Argv::unquoted("arg0"),
                    Argv::quoted("and arg1"),
                ],
                CommandBehavior::ExitOutput,
            ),
            ParsedCmd::new(vec![Argv::unquoted("this")], CommandBehavior::PipeOutput),
            ParsedCmd::new(vec![Argv::unquoted("tothis")], CommandBehavior::ExitOutput),
            ParsedCmd::new(
                vec![Argv::unquoted("thenthat")],
                CommandBehavior::ExitOutput,
            ),
        ];
        assert_eq!(actual, expected);
    }

    #[test]
    fn simple_multiple_command_mixed() {
        let input = "dothis;\"not;this\";this";
        let actual = match super::parse_argv_line(input) {
            Ok(s) => s,
            Err(e) => panic!("Error parsing: {}", e),
        };
        let expected = vec![
            ParsedCmd::new(vec![Argv::unquoted("dothis")], CommandBehavior::ExitOutput),
            ParsedCmd {
                argv: vec![Argv::quoted("not;this")],
                op: CommandBehavior::ExitOutput,
            },
            ParsedCmd::new(vec![Argv::unquoted("this")], CommandBehavior::ExitOutput),
        ];
        assert_eq!(actual, expected);
    }

    #[test]
    fn test_argv_line_comment() {
        let input = Span::new("//a comment");

        let (next, actual) = match super::argv_line_comment(input) {
            Ok(s) => s,
            Err(nom::Err::Error(e)) => {
                panic!(
                    "Error parsing: {} @ line {}, column {}\n{}\n{}",
                    e.message,
                    e.line(),
                    e.offset(),
                    input,
                    "-".repeat(e.offset() - 1) + "^"
                );
            }
            Err(e) => panic!("There was an error: {}", e),
        };
        assert_eq!(next.to_string(), "");

        //let expected = ParseResult::Comment(String::from("a comment"));
        //assert_eq!(actual.result, expected);
        let expected = "a comment";
        assert_eq!(actual.to_string(), expected);
    }

    #[test]
    fn test_unquoted_literal_with_line_comment() {
        let input = "unquoted and \"quoted arg\" //a comment";

        let actual = super::parse_argv_line(input).unwrap();
        let expected = vec![ParsedCmd::new(
            vec![
                Argv::unquoted("unquoted"),
                Argv::unquoted("and"),
                Argv::quoted("quoted arg"),
            ],
            CommandBehavior::ExitOutput,
        )];

        assert_eq!(actual, expected);
    }
}
