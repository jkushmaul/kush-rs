use criterion::{black_box, criterion_group, criterion_main, Criterion};
use kush_lang::parse_argv_line;

pub fn criterion_benchmark_parse_argv(name: &str, argv_line: &str, c: &mut Criterion) {
    c.bench_function(name, |b| {
        b.iter(|| {
            let _ = parse_argv_line(black_box(argv_line)).unwrap();
        })
    });
}

fn criterion_benchmark_kitchen_sink(c: &mut Criterion) {
    criterion_benchmark_parse_argv("parsing kitchen sink argv", "isolated command; with \"quotes\" | and pipes; and $vars, $$escapes, and ${braces} or ${braces ?} with options", c);
}

criterion_group! {
        name = benches;
    // This can be any expression that returns a `Criterion` object.
    config = Criterion::default().significance_level(0.1).sample_size(10000);
    targets = criterion_benchmark_kitchen_sink
}
criterion_main!(benches);
