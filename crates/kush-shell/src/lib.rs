#![warn(rustdoc::broken_intra_doc_links)]
#![warn(missing_docs)]
#![warn(rustdoc::missing_crate_level_docs)]
#![warn(rustdoc::private_doc_tests)]
#![warn(rustdoc::invalid_codeblock_attributes)]
#![warn(rustdoc::bare_urls)]
#![warn(rustdoc::invalid_html_tags)]

//! Kush is not an operating system shell. It's meant to be used as an CLI within an app
//! to provide a shell like experience
//! commands, aliases, variable substitution, output pipelines are all supported

pub mod builtin;
pub mod command;
pub mod variable;

mod error;
mod io;
mod shell;

use command::CommandProvider;
pub use error::KushError;
pub use io::KStdio;
pub use io::ReadReceiver;
pub use io::WriteSender;
pub use shell::Kush;

/// The state necessary for shell execution
pub struct ShellCtx<'a, CP: CommandProvider> {
    /// For printing and reading, can be piped
    pub stdio: &'a mut KStdio,

    /// The state
    pub inner: &'a mut CP::T,

    /// The shell
    pub shell: &'a mut Kush<CP>,
}

/// Allows anything to provide a mutable shell context
pub trait ShellCtxProvider<'a, CP: CommandProvider> {
    /// Provides mutable refs to the shell ctx parts
    fn get_shell_ctx(&mut self) -> ShellCtx<'_, CP>;
}

/// The most basic shell ctx provider
pub struct SimpleShellCtx<CP: CommandProvider> {
    /// Kstdio
    pub stdio: KStdio,

    /// Shell
    pub shell: Kush<CP>,

    /// State
    pub inner: CP::T,
}

impl<CP: CommandProvider> ShellCtxProvider<'_, CP> for SimpleShellCtx<CP> {
    fn get_shell_ctx(&mut self) -> ShellCtx<'_, CP> {
        ShellCtx {
            stdio: &mut self.stdio,
            shell: &mut self.shell,
            inner: &mut self.inner,
        }
    }
}

// **************************************************************

#[cfg(test)]
/// Testing utilities
pub(crate) mod test_utils {
    use crate::{
        command::{map_cp::HashMapCommandProvider, CommandExecutor, CommandProvider, CommandType},
        io::{ReadReceiver, WriteSender},
        shell::{
            pipeline::{Pipeline, PipelineCommand},
            KushAlias,
        },
        variable::MapVariableProvider,
        KStdio, Kush, KushError, ShellCtx, ShellCtxProvider, SimpleShellCtx,
    };
    use futures::channel::mpsc::{self};
    use kush_lang::parsed::{
        argv::Argv,
        tokenized::{ArgvRange, Token, Variable, VariableFlag},
        CommandBehavior,
    };
    use std::io::BufReader;

    pub struct TestCmd {}
    impl<CP: CommandProvider<T = String>> CommandExecutor<CP> for TestCmd {
        fn execute(ctx: &mut ShellCtx<CP>, argv: &[String]) -> Result<(), KushError> {
            *ctx.inner = format!("{:?}", argv);
            Ok(())
        }
    }

    /// Used to quickly make a test shell
    pub fn make_sh_ctx() -> SimpleShellCtx<HashMapCommandProvider<MapVariableProvider<String>>> {
        let (tx, rx) = mpsc::channel(1024);
        let tx = Box::new(WriteSender::from(tx));
        let rx = BufReader::new(ReadReceiver::from(rx));
        let rx = Box::new(rx);
        let stdio = KStdio::new(Some(rx), tx);
        let variable_provider = MapVariableProvider::default();
        let mut command_provider = HashMapCommandProvider::default();
        let cmd = CommandType::with_execute(
            "test_cmd".to_string(),
            String::new(),
            String::new(),
            TestCmd::execute,
        );

        let inner = String::new();
        command_provider.register_command(&inner, cmd).unwrap();
        let shell = Kush::new(variable_provider, command_provider).unwrap();

        SimpleShellCtx {
            inner,
            stdio,
            shell,
        }
    }

    /*
    So this little pile of shit - is due to the "alias" - quotes *included in that context...
    Vs just capturing what is inside of quotes, we have to also know that it was wrapped in quotes to start with. There's usually shell options or something to help with this.

    // This alias
    alias if_client "${* asis}";
    ->  When I run "if_client"  - take the args - evaluate them, so $$* turns into $*,
    if_client "alias" qc_cmd_cl "echo $$*";
    -> So this should turn "alias" qc_cmd_cl "echo $$*" into
       "alias" qc_cmd_cl "echo $$*"
       But because of commons sense, the double quotes are tossed aside and result is:
       alias qc_cmd_cl "echo $$*"

        // data/xonotic-data.pk3dir/commands.cfg
        // if_client    "alias" qc_cmd_cl     "cl_cmd $$*"
        let cmd = "

     */
    #[test]
    fn test_problematic_use_cases() {
        let mut ctx = make_sh_ctx();
        let mut ctx = ctx.get_shell_ctx();

        //If you want to use the builtins, you have to register them manually
        ctx.shell.register_builtins(ctx.inner).unwrap();

        // data/xonotic-data.pk3dir/commands.cfg
        // alias if_client "${* asis}"
        let cmd = "alias if_client \"${* asis}\"";

        if let Err(e) = Kush::execute(&mut ctx, cmd) {
            panic!("Error: {e}");
        }
        let actual = ctx.shell.get_alias("if_client").unwrap();
        let expected = KushAlias {
            description: String::new(),
            command_string: "${* asis}".to_string(),
            pipelines: vec![Pipeline {
                pipeline: vec![PipelineCommand {
                    argv: vec![(
                        Argv::unquoted("${* asis}"),
                        Token::Variable(
                            Variable::ArgvMulti(ArgvRange::start(1)),
                            VariableFlag::AsIs,
                        ),
                    )],
                    op: CommandBehavior::ExitOutput,
                }],
            }],
        };
        assert_eq!(actual, &expected);

        // data/xonotic-data.pk3dir/commands.cfg
        // if_client    "alias" qc_cmd_cl     "cl_cmd $$*"
        let cmd = "if_client    \"alias\" qc_cmd_cl     \"echo $$*\"";
        // This should have ran
        //       "alias" qc_cmd_cl     "cl_cmd $$*"

        println!("Executing {cmd}");
        if let Err(e) = Kush::execute(&mut ctx, cmd) {
            panic!("Error: {e}");
        }

        let actual = ctx.shell.get_alias("qc_cmd_cl").unwrap();
        let expected = KushAlias {
            description: String::new(),
            command_string: "echo $*".to_string(),
            pipelines: vec![Pipeline {
                pipeline: vec![PipelineCommand {
                    op: CommandBehavior::ExitOutput,
                    argv: vec![
                        (Argv::unquoted("echo"), Token::literal("echo")),
                        (
                            Argv::unquoted("$*"),
                            Token::Variable(
                                Variable::ArgvMulti(ArgvRange::start(1)),
                                VariableFlag::None,
                            ),
                        ),
                    ],
                }],
            }],
        };
        assert_eq!(actual, &expected);

        let cmd = "qc_cmd_cl hud clickradar";
        println!("Executing {cmd}");
        if let Err(e) = Kush::execute(&mut ctx, cmd) {
            panic!("Error executing '{cmd}': {e}");
        }

        println!("Done");
    }
}
