use futures::channel::mpsc::{self, TryRecvError, TrySendError};
use kush_lang::error::KushParseError;
use thiserror::Error;

/// All possible errors from kush
#[derive(Error, Debug)]
pub enum KushError {
    /// A syntax error on the command line
    #[error(transparent)]
    ParseError(#[from] KushParseError),
    ///The arguments to the command were invalid
    #[error(transparent)]
    InvalidArguments(#[from] clap::Error),
    ///If a command uses regex, this indicates invalid regex
    #[error(transparent)]
    RegexError(#[from] regex::Error),
    ///There was an error sending to the output
    #[error(transparent)]
    AsyncError(#[from] mpsc::SendError),
    ///There was an error sending to the output
    #[error(transparent)]
    StdoutError(#[from] TrySendError<Vec<u8>>),
    ///There was a std::io error
    #[error(transparent)]
    Io(#[from] std::io::Error),
    ///There was a parsing error
    #[error("Parse error: {0}")]
    ParserError(String),
    ///A variable, command, or alias was attempted to be registered twice
    #[error("Duplicate name: {0}")]
    DuplicateName(String),
    ///A command was not found
    #[error("Command not found: '{0}'")]
    CommandNotFound(String),
    ///There was an error within a command
    #[error("Command error: {0}")]
    CommandError(String),
    ///There was an sync error
    #[error("Sync error: {0}")]
    SyncError(String),
    ///There was an error reading from the stdin receiver, if one was present
    #[error(transparent)]
    StdinError(#[from] TryRecvError),
    ///Variables with the ! modifier require them to exist or fail evaluation
    #[error("Missing a required variable {0}")]
    MissingRequiredVariable(String),
    /// max recursion encountered
    #[error("Command {0} caused stack over flow ({1})")]
    StackOverflow(String, usize),
}
