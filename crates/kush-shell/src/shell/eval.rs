use crate::{variable::VariableProvider, KushError};
use kush_lang::parsed::{
    argv::Argv,
    tokenized::{ArgvRange, Token, Variable, VariableFlag},
};

const ARGV_NAMES: &[&str] = &[
    "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15",
];

pub struct Evaluator<'a, VP: VariableProvider> {
    vp: &'a VP,
    state: &'a VP::T,
    // Needs to know if they were quoted or not - because of bad acting "alias" behavior..
    arg_vars: &'a [Argv],
}

fn eval_escape(quoted: bool, s: &str) -> String {
    if quoted && s.contains(['\\', '\"']) {
        return s.replace("\\", "\\\\").replace("\"", "\\\"");
    }
    s.to_string()
}

impl<'a, VP: VariableProvider> Evaluator<'a, VP> {
    fn var_named(&self, name: &str, flag: &VariableFlag) -> Result<&str, KushError> {
        let required = matches!(flag, VariableFlag::Required);

        let value = if let Some(v) = ARGV_NAMES
            .iter()
            .enumerate()
            .find(|(_, &s)| s == name)
            .and_then(|(i, _)| self.arg_vars.get(i))
        {
            Some(v.value())
        } else {
            self.vp.get_variable(self.state, name)
        };

        if required && value.is_none() {
            return Err(KushError::MissingRequiredVariable(name.to_string()));
        }
        Ok(value.unwrap_or_default())
    }

    fn var_argv_single(&self, index: usize, flag: &VariableFlag) -> Result<&str, KushError> {
        let required = matches!(flag, VariableFlag::Required);

        let value = match self.arg_vars.get(index) {
            Some(v) => v.value(),
            None if required => {
                return Err(KushError::MissingRequiredVariable(index.to_string()));
            }
            None => "",
        };

        Ok(value)
    }

    // This does add new argv, so it returns Argv
    fn eval_var_argv_range(
        &self,
        range: &ArgvRange,
        flag: &VariableFlag,
    ) -> Result<Vec<Argv>, KushError> {
        let argv = self
            .arg_vars
            .iter()
            .skip(range.get_start())
            .take(range.get_len().unwrap_or(self.arg_vars.len()));

        // Multi defaults to AsIs
        let quoted = matches!(flag, VariableFlag::Quoted);

        let mut final_args = vec![];
        for actual in argv {
            // Given: argv: ["an", "\"alias\"", "with quotes"]
            // Then parsed argv should become:
            //        [ Argv::unquoted("an"), Argv::quoted("alias"), Argv::quoted("with quotes") ]
            // And eval of
            //    ${* q}
            //          Should become:   [ Argv::unquoted("an"), Argv::quoted("alias"), Argv::quoted("with quotes") ]
            //    ${* asis}
            //          Should become: ["an \"alias\" \"with quotes\"" ->     ["an","alias",  "\"with quotes\""]
            let s = actual.value();

            let s = if quoted {
                eval_escape(true, s)
            } else {
                s.to_string()
            };
            let s = if actual.is_quoted() {
                Argv::Quoted(s)
            } else {
                Argv::Unquoted(s)
            };
            final_args.push(s);
        }

        //Handle flag with default AsIs

        Ok(final_args)
    }

    // Has the potential to add argv, so Vec<String>...
    fn eval_var(
        &self,
        was_quoted: bool,
        var: &Variable,
        flag: &VariableFlag,
        level: usize,
    ) -> Result<Vec<Argv>, KushError> {
        let value = match var {
            Variable::Eval(e) => {
                let n = self.sequence(e, level + 1)?;
                self.var_named(&n, flag)?
            }
            Variable::Named(n) => self.var_named(n, flag)?,
            Variable::ArgvExact(n) => self.var_argv_single(*n, flag)?,

            Variable::ArgvMulti(argv_range) => {
                // Unique in that it adds to results for each in range vs one single string
                return self.eval_var_argv_range(argv_range, flag);
            }
        };

        // If flag was provided, use that, or allow default
        let quoted = !matches!(flag, VariableFlag::AsIs);

        let value = if quoted {
            eval_escape(quoted, value)
        } else {
            value.to_string()
        };

        let value = if was_quoted {
            Argv::Quoted(value)
        } else {
            Argv::Unquoted(value)
        };

        Ok(vec![value])
    }

    fn sequence(&self, sequence: &[Token], level: usize) -> Result<String, KushError> {
        let mut results = String::new();
        for t in sequence {
            for s in self.eval_token(t, false, level + 1)? {
                results.push_str(s.value());
            }
        }

        Ok(results)
    }

    /// Given an argv, represented by token, evaluate from vars; possibly expanding into multiple args
    pub fn eval_token(
        &self,
        token: &Token,
        is_quoted: bool,
        level: usize,
    ) -> Result<Vec<Argv>, KushError> {
        let s = match token {
            Token::Literal(s) => s.to_string(),
            Token::Sequence(seq) => self.sequence(seq, level + 1)?,
            Token::Variable(v, f) => {
                return self.eval_var(is_quoted, v, f, level + 1);
            }
            Token::Empty => String::new(),
        };

        let result = if is_quoted {
            Argv::Quoted(s)
        } else {
            Argv::Unquoted(s)
        };
        Ok(vec![result])
    }

    pub fn new(vp: &'a VP, state: &'a VP::T, arg_vars: &'a [Argv]) -> Self {
        Self {
            vp,
            state,
            arg_vars,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::Evaluator;
    use crate::{
        shell::eval::eval_escape,
        variable::{MapVariableProvider, VariableProvider},
    };
    use kush_lang::parsed::{
        argv::Argv,
        tokenized::{ArgvRange, Token, Variable, VariableFlag},
    };

    #[test]
    fn test_eval_literal() {
        let token = Token::Literal("literal".to_string());
        let vp = MapVariableProvider::default();
        let state = String::new();
        let arg_vars = &[];
        let evaluator = Evaluator::new(&vp, &state, arg_vars);
        let actual = evaluator.eval_token(&token, false, 0).unwrap();
        let expected = vec![Argv::unquoted("literal")];
        assert_eq!(actual, expected);
    }

    #[test]
    fn test_eval_literal_quoted() {
        let token = Token::Literal("literal".to_string());
        let vp = MapVariableProvider::default();
        let state = String::new();
        let arg_vars = &[];
        let evaluator = Evaluator::new(&vp, &state, arg_vars);
        let actual = evaluator.eval_token(&token, true, 0).unwrap();
        let expected = vec![Argv::quoted("literal")];
        assert_eq!(actual, expected);
    }

    #[test]
    fn test_eval_sequence() {
        let token = Token::Sequence(vec![
            Token::Literal("one".to_string()),
            Token::Literal("two".to_string()),
        ]);
        let vp = MapVariableProvider::default();
        let state = String::new();
        let arg_vars = &[];
        let evaluator = Evaluator::new(&vp, &state, arg_vars);
        let actual = evaluator.eval_token(&token, false, 0).unwrap();
        let expected = vec![Argv::unquoted("onetwo")];
        assert_eq!(actual, expected);
    }

    #[test]
    fn test_eval_var_named_quoted() {
        let mut state = String::new();
        let mut vp = MapVariableProvider::default();
        let varname = "var";
        let var_value = "spaces and \\ and \"quotes\"";
        vp.set_variable(&mut state, varname.to_string(), var_value.to_string())
            .unwrap();

        let arg_vars = &[];
        let evaluator = Evaluator::new(&vp, &state, arg_vars);

        let token = Token::Variable(Variable::Named(varname.to_string()), VariableFlag::None);

        // Default for named is to be quoted
        let actual = evaluator.eval_token(&token, false, 0).unwrap();
        let expected = vec![Argv::Unquoted(eval_escape(true, var_value))];
        assert_eq!(actual, expected);
    }

    #[test]
    fn test_eval_var_named_asis() {
        let mut state = String::new();
        let mut vp = MapVariableProvider::default();
        let varname = "var";
        let var_value = "spaces and \\ and \"quotes\"";
        vp.set_variable(&mut state, varname.to_string(), var_value.to_string())
            .unwrap();

        let arg_vars = &[];
        let evaluator = Evaluator::new(&vp, &state, arg_vars);

        let token = Token::Variable(Variable::Named(varname.to_string()), VariableFlag::AsIs);

        let actual = evaluator.eval_token(&token, false, 0).unwrap();
        let expected = vec![Argv::unquoted(var_value)];
        assert_eq!(actual, expected);
    }

    #[test]
    fn test_eval_var_named_optional_missing_is_ok() {
        let state = String::new();
        let vp = MapVariableProvider::default();
        let varname = "var";

        let arg_vars = &[];
        let evaluator = Evaluator::new(&vp, &state, arg_vars);

        let token = Token::Variable(Variable::Named(varname.to_string()), VariableFlag::None);

        let actual = evaluator.eval_token(&token, false, 0).unwrap();
        assert_eq!(actual, &[Argv::unquoted("")]);
    }

    #[test]
    fn test_eval_var_named_required_missing_is_err() {
        let state = String::new();
        let vp = MapVariableProvider::default();
        let varname = "var";

        let arg_vars = &[];
        let evaluator = Evaluator::new(&vp, &state, arg_vars);

        let token = Token::Variable(Variable::Named(varname.to_string()), VariableFlag::Required);

        let actual = evaluator.eval_token(&token, false, 0);
        assert!(actual.is_err());
    }

    #[test]
    fn test_eval_var_argv_exact_quoted() {
        let state = String::new();
        let vp = MapVariableProvider::default();
        let var_value = "spaces and \"quotes\"";

        let arg_vars = &[Argv::unquoted("command"), Argv::unquoted(var_value)];
        let evaluator = Evaluator::new(&vp, &state, arg_vars);

        let token = Token::Variable(Variable::ArgvExact(1), VariableFlag::None);

        // Default for exact argv is quoted (no different than named)
        let actual = evaluator.eval_token(&token, false, 0).unwrap();
        let expected = vec![Argv::Unquoted(eval_escape(true, var_value))];

        assert_eq!(actual, expected);
    }

    #[test]
    fn test_eval_var_argv_multi_all_asis() {
        let state = String::new();
        let vp = MapVariableProvider::default();

        let arg_vars = vec![
            Argv::unquoted("command"),
            Argv::unquoted("a one"),
            Argv::unquoted("ana two"),
            Argv::unquoted("ana three"),
        ];
        let evaluator = Evaluator::new(&vp, &state, &arg_vars);

        let token = Token::Variable(Variable::ArgvMulti(ArgvRange::start(1)), VariableFlag::None);
        let actual = evaluator.eval_token(&token, false, 0).unwrap();

        // Multi args default to asis
        let expected: Vec<_> = arg_vars.into_iter().skip(1).collect();
        assert_eq!(actual, expected);
    }

    #[test]
    fn test_eval_var_argv_multi_all_quoted() {
        let state = String::new();
        let vp = MapVariableProvider::default();

        let arg_vars = vec![
            Argv::unquoted("command"),
            Argv::unquoted("a one"),
            Argv::unquoted("ana two"),
            Argv::unquoted("ana three"),
        ];
        let evaluator = Evaluator::new(&vp, &state, &arg_vars);

        let token = Token::Variable(
            Variable::ArgvMulti(ArgvRange::start(1)),
            VariableFlag::Quoted,
        );

        let actual = evaluator.eval_token(&token, false, 0).unwrap();
        // Multi args default to asis

        let expected: Vec<_> = arg_vars
            .into_iter()
            .skip(1)
            .map(|s| Argv::Unquoted(eval_escape(true, s.value())))
            .collect();
        assert_eq!(actual, expected);
    }

    /*
    #[test]
    fn test_eval_token() {
        let mut ctx = test_utils::make_sh();

        ctx.shell
            .get_variable_provider_mut()
            .set_variable(&mut ctx.inner, "test".to_string(), "pass".to_string())
            .unwrap();

        let vars = vec![];
        // Assert that variables aren't always used if literal:
        let token = ParseToken::Literal("test".to_string());
        let result = ctx.shell.eval_token(&ctx.inner, &token, &vars, 0).unwrap();
        assert_eq!(result, vec!["test"]);

        // Assert that a variable parse result can be evaluated
        let token = ParseToken::Variable(TokenVariable {
            name: VariableName::Named("test".to_string()),
            modifier: None,
        });

        let result = ctx.shell.eval_token(&ctx.inner, &token, &vars, 0).unwrap();
        assert_eq!(result, vec!["pass"]);

        // For good measure assert both
        let token = ParseToken::Sequence(Vec::from_iter([
            ParseToken::Literal("test".to_string()),
            ParseToken::Literal(": ".to_string()),
            ParseToken::Variable(TokenVariable {
                name: VariableName::Named("test".to_string()),
                modifier: None,
            }),
        ]));
        let result = ctx.shell.eval_token(&ctx.inner, &token, &vars, 0).unwrap();
        assert_eq!(result, vec!["test: pass"]);
    }

    #[test]
    pub fn test_eval_args() {
        let mut ctx = test_utils::make_sh();

        let vp = ctx.shell.get_variable_provider_mut();
        let mut set_var = |name: &str, val: &str| {
            vp.set_variable(&mut ctx.inner, name.to_string(), val.to_string())
                .unwrap();
        };

        let argv: Vec<String> = ["zero", "one", "two", "three", "four", "five"]
            .into_iter()
            .enumerate()
            .map(|(i, v)| {
                let n = i.to_string();
                set_var(&n, v);
                v.to_string()
            })
            .collect();

        let orig = "$3";
        let expected = ["three"];
        let token = crate::lang::parse_tokens(orig).unwrap();
        let actual = ctx.shell.eval_token(&ctx.inner, &token, &argv, 0).unwrap();

        assert_eq!(
            actual, expected,
            "\n  expected {}\n  {:#?}\n  to become {:?},\n  but got {:?}",
            orig, token, expected, actual
        );

        let orig = "${3-}";
        let expected = ["three", "four", "five"];
        let token = crate::lang::parse_tokens(orig).unwrap();
        let actual = ctx.shell.eval_token(&ctx.inner, &token, &argv, 0).unwrap();

        assert_eq!(
            actual, expected,
            "\n  expected {}\n  {:#?}\n  to become {:?},\n  but got {:?}",
            orig, token, expected, actual
        );

        let orig = "quoted ${3-}";
        let expected = ["quoted three four five"];
        let token = crate::lang::parse_tokens(orig).unwrap();
        let actual = ctx.shell.eval_token(&ctx.inner, &token, &argv, 0).unwrap();

        assert_eq!(
            actual, expected,
            "\n  expected {}\n  {:#?}\n  to become {:?},\n  but got {:?}",
            orig, token, expected, actual
        );
    }
    */
}
