mod alias;
pub mod eval;
mod kush;
pub mod pipeline;

pub use self::kush::Kush;
pub use alias::KushAlias;

// **************************************************************

fn strip_line(line: &str) -> String {
    let mut line = line.to_owned();

    if line.ends_with("\r") {
        line.pop();
    }
    line
}

/// A lame struct used to indicate if an identifier is used across 3 the 3 providers
/// It is very possible all 3 can be true - depending on the provider
pub struct Identifiers {
    /// It is a variable
    pub variable: bool,
    /// It is a command
    pub command: bool,
    /// It is an alias
    pub alias: bool,
}
