use super::{
    eval::Evaluator,
    pipeline::{Pipeline, PipelineCommand},
    Identifiers, KushAlias,
};
use crate::{
    command::CommandProvider,
    error::KushError,
    io::{ReadReceiver, WriteSender},
    variable::{ShadowVariableProvider, VariableProvider},
    ShellCtx,
};
use futures::channel::mpsc::{self};
use kush_lang::parsed::argv::Argv;
use std::{collections::HashMap, io::BufReader};

/// The core object for shell processing
pub struct Kush<CP: CommandProvider> {
    // Aliases are simply pre-parsed command lines, ready to be evaluated on demand
    // Considering shifting these into command provider though...
    aliases: HashMap<String, KushAlias>,
    // Actual commands ready to be executed
    command_provider: CP,
    // Container for any variables to provide to the evaluation
    variable_provider: ShadowVariableProvider<CP::VP>,

    stack_entry_cmdline: Option<String>,
    stack_count: usize,
    stack_max: usize,
}

impl<CP: CommandProvider> Kush<CP> {
    /// Create a new shell, with given stdout transmitter (caller owns rx) and builtin registration data, with var/cmd providers
    pub fn new(variable_provider: CP::VP, command_provider: CP) -> Result<Self, KushError> {
        let variable_provider = ShadowVariableProvider::new(variable_provider);
        let s = Self {
            aliases: HashMap::new(),
            command_provider,
            variable_provider,
            stack_entry_cmdline: None,
            stack_count: 0,
            stack_max: 8,
        };

        Ok(s)
    }

    /// Get a list of shell aliases
    pub fn get_aliases(&self) -> &HashMap<String, KushAlias> {
        &self.aliases
    }

    /// Max recursion, 0 == no recursion, 1 call only...
    pub fn set_stack_max(&mut self, stack_max: usize) {
        self.stack_max = stack_max;
    }

    /// Return a list of identifiers with this name
    pub fn get_identifiers(&self, state: &CP::T, name: &str) -> Identifiers {
        Identifiers {
            variable: self.command_provider.contains_command(state, name),
            command: self.variable_provider.contains_variable(state, name),
            alias: self.aliases.contains_key(name),
        }
    }

    /// Get the command provider
    pub fn get_command_provider(&self) -> &CP {
        &self.command_provider
    }

    /// Get the variable provider
    pub fn get_variable_provider(&self) -> &ShadowVariableProvider<CP::VP> {
        &self.variable_provider
    }

    /// Get a mutable reference to cmd provider
    pub fn get_command_provider_mut(&mut self) -> &mut CP {
        &mut self.command_provider
    }

    /// Get a mutable reference to the var provider
    pub fn get_variable_provider_mut(&mut self) -> &mut ShadowVariableProvider<CP::VP> {
        &mut self.variable_provider
    }

    /**
    Token, is the tokenized for of the command line.
    It must be evaluated to a resulting Vec<String> to get argv
    If argv[0] is an alias:
        evaluate the alias pipeline, using this argv which produces a new pipeline.
        execute that pipeline (it may not be just one command - it can be a full chain of pipes etc that re-enter this function)
    else:
        execute the command with the argv (The alias eventually makes it here)

     */
    /// If the last command op was pipe, then stdin_rx, will be the stdout_rx from that process
    /// This function will then return, the output rx
    fn execute_pipeline_command(
        ctx: &mut ShellCtx<CP>,
        pipeline_cmd: PipelineCommand,
        parent_argv: &[Argv],
    ) -> Result<(), KushError> {
        // Because of alias totally screwing up the flow... We cannot evaluate until we know it's not the alias command...

        let (argv_0, token_0) = match pipeline_cmd.argv.first() {
            Some(a) => a,
            None => return Ok(()),
        };
        let evaluator = Evaluator::new(ctx.shell.get_variable_provider(), ctx.inner, parent_argv);
        let mut parsed_argv = evaluator.eval_token(token_0, argv_0.is_quoted(), 0)?;
        let argv_0 = match parsed_argv.first().filter(|a| !a.value().is_empty()) {
            Some(a) => a,
            None => return Ok(()),
        };

        // alias - when not quoted - does not actually evaluate the args, which is annoying.
        // else, the args get evaluated
        let argv: Vec<Argv> = if argv_0.value() == "alias" && !argv_0.is_quoted() {
            // Do not evaluate
            pipeline_cmd.argv.into_iter().map(|(a, _)| a).collect()
        } else {
            // Do evaluate, and, we already evaluated the first arg
            for (argv, token) in pipeline_cmd.argv.into_iter().skip(1) {
                // This needs to skip the first one, we already did that.
                parsed_argv.extend_from_slice(&evaluator.eval_token(
                    &token,
                    argv.is_quoted(),
                    0,
                )?);
            }
            parsed_argv
        };

        let argv_0 = match argv.first().filter(|a| !a.value().is_empty()) {
            Some(a) => a,
            None => return Ok(()),
        };

        if let Some(alias) = ctx.shell.get_alias(argv_0.value()) {
            let pipelines = alias.pipelines.clone();
            return Self::execute_pipelines(ctx, pipelines, &argv);
        }

        // Look at alias first, because get_command may return a default command handler
        if !ctx
            .shell
            .get_command_provider()
            .contains_command(ctx.inner, argv_0.value())
        {
            // Then check if it is an alias
            if let Some(alias) = ctx.shell.aliases.get(argv_0.value()) {
                let pipelines = alias.pipelines.clone();
                // And pass this pipeline's argv, as the parent argv
                return Self::execute_pipelines(ctx, pipelines, &argv);
            }
        }

        let cmd =  match ctx.shell.command_provider.get_command(ctx.inner, argv_0.value()) {
            Some(c) => c,
            None => return Err(KushError::CommandNotFound(format!("Could not find command with name '{:?}', nor could an alias be found of same name, command not found: {:#?}", argv_0, argv))),
        };
        let cmd_execute = cmd.get_execute();

        //TODO: Move the "build_env" bit, INTO the command as an etra trait function (like execute)
        //      And therefore, it allows the command to decide if it evaluates or not, so the above logic for alias, would become a part of the alias command itself.
        //  And I think that means it is taking Vec<Argv> now, not Vec<String>; so that the command can decide what to do based on original quoting...
        let argv: Vec<String> = argv.into_iter().map(|a| a.into_inner()).collect();

        let vp = ctx.shell.get_variable_provider_mut();
        vp.reset();
        for (argc, argv) in argv.iter().enumerate() {
            let name = format!("{}", argc);
            vp.set_shadowed(name, argv.to_string());
        }

        // Do not short circuit - clear the vp first
        let result = (cmd_execute)(ctx, &argv);
        ctx.shell.get_variable_provider_mut().reset();

        // Now return the result
        result
    }

    // Fully executes a pipeline with final output to provided stdio.
    fn execute_pipelines(
        ctx: &mut ShellCtx<CP>,
        pipelines: impl IntoIterator<Item = Pipeline>,
        parent_argv: &[Argv],
    ) -> Result<(), KushError> {
        let mut original_stdout_tx = None;

        if ctx.shell.stack_count > ctx.shell.stack_max {
            return Err(KushError::StackOverflow(
                ctx.shell.stack_entry_cmdline.take().unwrap_or_default(),
                ctx.shell.stack_count,
            ));
        }
        ctx.shell.stack_count += 1;

        let mut result = Ok(());

        for mut pipeline in pipelines {
            if result.is_err() {
                break;
            }
            if pipeline.pipeline.is_empty() {
                continue;
            }

            let last = pipeline.pipeline.len() - 1;
            let exit = pipeline.pipeline.remove(last);

            //Iterate all but last, without returning due to error etc.
            for pipe in pipeline.pipeline {
                let (new_stdout_tx, new_stdin_rx) = mpsc::channel::<Vec<u8>>(1024);
                let new_stdout_tx = WriteSender::from(new_stdout_tx);
                let new_stdout_tx = Box::new(new_stdout_tx);

                // The output was already writtten so there is no need to write anymore - unless it's the root stdout which will be wrtten all final after loop
                let old_stdout_tx = ctx.stdio.pipe(new_stdout_tx);
                if original_stdout_tx.is_none() {
                    original_stdout_tx = Some(old_stdout_tx);
                }

                // ctx.stdio.stdout is now a writer to new_stdin_rx
                result = Self::execute_pipeline_command(ctx, pipe, parent_argv);
                if result.is_err() {
                    break;
                }

                // Create a reader for new_stdin_rx to relay writter data to it
                let new_stdin_rx = BufReader::new(ReadReceiver::from(new_stdin_rx));
                let new_stdin_rx = Box::new(new_stdin_rx);
                let _ = ctx.stdio.stdin.insert(new_stdin_rx);
            }

            //Return original stdout to stdio
            if let Some(original_stdout_tx) = original_stdout_tx.take() {
                ctx.stdio.pipe(original_stdout_tx);
            }
            // and exit if err
            if result.is_err() {
                break;
            }

            // And exectute the final step in this pipe
            result = Self::execute_pipeline_command(ctx, exit, parent_argv);
            if result.is_err() {
                break;
            }
        }

        if ctx.shell.stack_count > 0 {
            ctx.shell.stack_count -= 1;
        }
        // reconnect original stdout if the pipe was interrupted
        if let Some(original_stdout_tx) = original_stdout_tx.take() {
            ctx.stdio.pipe(original_stdout_tx);
        }

        result
    }
    /*
        // Fully executes a pipeline with final output to provided stdio.
        fn execute_pipelines2(
            ctx: &mut ShellCtx<CP>,
            pipelines: impl IntoIterator<Item = Pipeline>,
            parent_argv: &[String],
        ) -> Result<(), KushError> {
            let mut original_stdout_tx = None;

            if ctx.shell.stack_count > ctx.shell.stack_max {
                return Err(KushError::StackOverflow(
                    ctx.shell.stack_entry_cmdline.take().unwrap_or_default(),
                    ctx.shell.stack_count,
                ));
            }
            ctx.shell.stack_count += 1;

            let mut result = Ok(());

            for mut pipeline in pipelines {
                if result.is_err() {
                    break;
                }
                if pipeline.pipeline.is_empty() {
                    continue;
                }

                let last = pipeline.pipeline.len() - 1;
                let exit = pipeline.pipeline.remove(last);

                //Iterate all but last
                for pipe in pipeline.pipeline {
                    let (new_stdout_tx, new_stdin_rx) = mpsc::channel::<Vec<u8>>(1024);
                    let new_stdout_tx = WriteSender::from(new_stdout_tx);
                    let new_stdout_tx = Box::new(new_stdout_tx);

                    // The output was already writtten so there is no need to write anymore - unless it's the root stdout which will be wrtten all final after loop
                    let old_stdout_tx = ctx.stdio.pipe(new_stdout_tx);
                    if original_stdout_tx.is_none() {
                        original_stdout_tx = Some(old_stdout_tx);
                    }

                    // ctx.stdio.stdout is now a writer to new_stdin_rx
                    result = Self::execute_parsed_argv_command(ctx, pipe, parent_argv);
                    if result.is_err() {
                        break;
                    }

                    // Create a reader for new_stdin_rx to relay writter data to it
                    let new_stdin_rx = BufReader::new(ReadReceiver::from(new_stdin_rx));
                    let new_stdin_rx = Box::new(new_stdin_rx) as Box<dyn BufRead>;
                    let _ = ctx.stdio.stdin.insert(new_stdin_rx);
                }

                //Now send output to pipe exit with original stdout_tx
                if let Some(original_stdout_tx) = original_stdout_tx.take() {
                    ctx.stdio.pipe(original_stdout_tx);
                }

                result = Self::execute_parsed_argv_command(ctx, exit, parent_argv);
                if result.is_err() {
                    break;
                }
            }

            if ctx.shell.stack_count > 0 {
                ctx.shell.stack_count -= 1;
            }
            // reconnect original stdout if the pipe was interrupted
            if let Some(original_stdout_tx) = original_stdout_tx.take() {
                ctx.stdio.pipe(original_stdout_tx);
            }

            result
        }
    */

    /// Execute a given shell string
    pub fn execute(ctx: &mut ShellCtx<CP>, line: &str) -> Result<(), KushError> {
        let pipeline = match Pipeline::parse(line)? {
            Some(p) if !p.is_empty() => p,
            _ => return Ok(()),
        };

        if ctx.shell.stack_count == 0 {
            ctx.shell.stack_entry_cmdline = Some(line.to_string());
        }

        Self::execute_pipelines(ctx, pipeline, &[])?;

        Ok(())
    }

    /// Register builtins - this should only be performed once, so it consumes self and returns it back
    pub fn register_builtins(&mut self, state: &mut CP::T) -> Result<(), KushError> {
        for c in crate::builtin::all_builtin().unwrap().into_iter() {
            self.get_command_provider_mut().register_command(state, c)?
        }

        Ok(())
    }

    /// Parse + Register an alias
    pub fn register_alias(
        &mut self,
        alias_name: String,
        command_string: String,
        description: String,
    ) -> Result<(), KushError> {
        // eprintln!("Registering alias[{alias_name}] -> '{command_string}'");
        let pipelines = Pipeline::parse(&command_string)?.unwrap_or_default();
        let alias = KushAlias::new(pipelines, command_string, description);
        self.aliases.insert(alias_name, alias);
        Ok(())
    }

    /// Remove an alias
    pub fn remove_alias(&mut self, name: String) {
        self.aliases.remove(&name);
    }

    /// Quickly inspect an alias
    pub fn get_alias(&self, arg: &str) -> Option<&KushAlias> {
        self.aliases.get(arg)
    }
}

#[cfg(test)]
pub mod tests {
    use kush_lang::parsed::{
        argv::Argv,
        tokenized::{ArgvRange, Token, Variable, VariableFlag},
        CommandBehavior,
    };

    use crate::{
        command::{CommandProvider, CommandType},
        shell::{
            pipeline::{Pipeline, PipelineCommand},
            KushAlias,
        },
        test_utils,
        variable::VariableProvider,
        Kush, KushError, ShellCtx, ShellCtxProvider,
    };
    use std::io::BufRead;

    pub struct TestPipeCmd;

    impl TestPipeCmd {
        fn writer<CP: CommandProvider>(
            ctx: &mut ShellCtx<CP>,
            argv: &[String],
        ) -> Result<(), KushError> {
            let s = format!("{:?}", argv);
            ctx.stdio.println(s).unwrap();

            Ok(())
        }
        fn reader<CP: CommandProvider>(
            ctx: &mut ShellCtx<CP>,
            _: &[String],
        ) -> Result<(), KushError> {
            while let Ok(Some(line)) = ctx.stdio.readln() {
                ctx.stdio.println(format!("read: {}", line))?;
            }

            Ok(())
        }
    }

    #[test]
    fn test_pipe_exec() {
        let mut ctx = test_utils::make_sh_ctx();
        let mut ctx = ctx.get_shell_ctx();

        let mut stdout_rx = ctx.stdio.stdin.take().unwrap();

        let writer = CommandType::with_execute(
            "test_write".into(),
            "about".into(),
            "help".into(),
            TestPipeCmd::writer,
        );
        ctx.shell
            .get_command_provider_mut()
            .register_command(ctx.inner, writer)
            .unwrap();

        let reader = CommandType::with_execute(
            "test_read".into(),
            "about".into(),
            "help".into(),
            TestPipeCmd::reader,
        );
        // Create a useable command and register it

        ctx.shell
            .get_command_provider_mut()
            .register_command(ctx.inner, reader)
            .unwrap();

        Kush::execute(&mut ctx, "test_write | test_read").unwrap();

        // Check the output
        let mut actual = String::new();
        stdout_rx.read_line(&mut actual).unwrap();
        let expected = &["test_write"];
        let expected = format!("read: {:?}\n", expected);
        assert_eq!(&actual, &expected);
    }

    #[test]
    fn test_execute_alias_quoted_empty() {
        // Move this first half out to the right place...
        let mut ctx = test_utils::make_sh_ctx();
        let mut ctx = ctx.get_shell_ctx();
        let command_string = "test_cmd $1 \"${$1 ?}\"";
        ctx.shell
            .register_alias(
                "test_alias".to_string(),
                command_string.to_string(),
                String::new(),
            )
            .unwrap();

        // Create the alias
        let alias = ctx.shell.get_alias("test_alias").unwrap();
        // Make sure the command string was what we would expect
        assert_eq!(alias.command_string, "test_cmd $1 \"${$1 ?}\"");

        // argv[0]: seta
        // argv[1]: \"${$1 ?}\"
        let argv1 = Token::Variable(Variable::ArgvExact(1), VariableFlag::default());
        let argv = vec![
            (
                Argv::unquoted("test_cmd"),
                Token::Literal("test_cmd".to_string()),
            ),
            (Argv::unquoted("$1"), argv1.clone()),
            (
                Argv::quoted("${$1 ?}"),
                Token::Variable(Variable::Eval(vec![argv1]), VariableFlag::Optional),
            ),
        ];

        let expected = vec![Pipeline {
            pipeline: vec![PipelineCommand {
                argv,
                op: CommandBehavior::default(),
            }],
        }];
        assert_eq!(alias.pipelines, expected);

        let cmd = "test_alias test_var";
        if let Err(e) = Kush::execute(&mut ctx, cmd) {
            panic!("Failed to execute: {e}");
        }

        let expected_argv = vec![
            "test_cmd".to_string(),
            "test_var".to_string(),
            String::new(),
        ];
        let expected_state = format!("{:?}", expected_argv);
        assert_eq!(ctx.inner, &expected_state);
    }

    #[test]
    fn test_execute_alias_var_name() {
        let mut ctx = test_utils::make_sh_ctx();
        let mut ctx = ctx.get_shell_ctx();

        ctx.shell.register_builtins(ctx.inner).unwrap();

        ctx.shell
            .variable_provider
            .set_variable(ctx.inner, "VAR".to_string(), "TEST".to_string())
            .unwrap();

        let register_alias = "alias test_alias \"test_cmd $VAR\"";
        Kush::execute(&mut ctx, register_alias).unwrap();

        // Now execute it
        let execute_alias = "test_alias one two 3";
        ctx.inner.clear();
        Kush::execute(&mut ctx, execute_alias).unwrap();
        // And verify state
        let expected = format!("{:?}", ["test_cmd", "TEST"]);
        assert_eq!(ctx.inner, &expected);
    }

    #[test]
    fn test_execute_quoted_alias_var() {
        let mut ctx = test_utils::make_sh_ctx();
        let mut ctx = ctx.get_shell_ctx();

        ctx.shell.register_builtins(ctx.inner).unwrap();

        ctx.shell
            .variable_provider
            .set_variable(ctx.inner, "VAR".to_string(), "TEST".to_string())
            .unwrap();

        // Problem is that when eval is called, it just evals literal token "alias",
        // so
        let register_alias = "\"alias\" test_alias \"test_cmd $$VAR\"";
        // This is such a stupid way of doing it..
        if let Err(e) = Kush::execute(&mut ctx, register_alias) {
            panic!("{e}");
        }

        // Now execute it
        let execute_alias = "test_alias one two 3";
        ctx.inner.clear();
        Kush::execute(&mut ctx, execute_alias).unwrap();
        // And verify state
        let expected = format!("{:?}", ["test_cmd", "TEST"]);
        assert_eq!(ctx.inner, &expected);
    }

    #[test]
    fn test_execute_alias_nested() {
        let mut ctx = test_utils::make_sh_ctx();
        let mut ctx = ctx.get_shell_ctx();

        ctx.shell.register_builtins(ctx.inner).unwrap();

        let register_alias = "alias test_alias \"test_cmd ${* ?}\"";
        Kush::execute(&mut ctx, register_alias).unwrap();

        ctx.inner.clear();
        let execute_alias = "test_alias one two 3";
        Kush::execute(&mut ctx, execute_alias).unwrap();
        let actual = ctx.inner;
        let expected = format!("{:?}", vec!["test_cmd", "one", "two", "3"]);
        assert_eq!(actual, &expected);
    }

    #[test]
    fn test_execute_alias_asis() {
        let mut ctx = test_utils::make_sh_ctx();
        let mut ctx = ctx.get_shell_ctx();

        ctx.shell.register_builtins(ctx.inner).unwrap();

        // Quoted "alias" means "DO eval" - but on execution of the alias
        let wrapped_register_alias = "alias test_alias     \"test_cmd $*\"";
        Kush::execute(&mut ctx, wrapped_register_alias).unwrap();
        let alias = ctx.shell.get_alias("test_alias");
        assert!(alias.is_some());
        let alias = alias.unwrap();

        let argv = vec![
            (Argv::unquoted("test_cmd"), Token::literal("test_cmd")),
            (
                Argv::unquoted("$*"),
                Token::Variable(Variable::ArgvMulti(ArgvRange::start(1)), VariableFlag::None),
            ),
        ];
        let expected = KushAlias {
            description: String::new(),
            command_string: "test_cmd $*".to_string(),
            pipelines: vec![Pipeline {
                pipeline: vec![PipelineCommand {
                    argv,
                    op: CommandBehavior::ExitOutput,
                }],
            }],
        };

        assert_eq!(alias, &expected);

        let execute_alias = "test_alias  one two 3";
        Kush::execute(&mut ctx, execute_alias).unwrap();
        let actual = ctx.inner.split_off(0);
        let expected = format!("{:?}", vec!["test_cmd", "one", "two", "3"]);
        assert_eq!(actual, expected);
    }

    #[test]
    fn test_execute_alias_multiarg_quoted() {
        let mut ctx = test_utils::make_sh_ctx();
        let mut ctx = ctx.get_shell_ctx();

        ctx.shell.register_builtins(ctx.inner).unwrap();

        // Test Quoted
        Kush::execute(
            &mut ctx,
            "alias testalias \"test_cmd literalarg1 $2 ${3- q}\"",
        )
        .unwrap();
        Kush::execute(&mut ctx, "testalias   arg1 arg2 arg3 \"arg4 with spaces\"").unwrap();
        let actual = ctx.inner;
        let expected = format!(
            "{:?}",
            [
                "test_cmd",
                "literalarg1",
                "arg2",
                "arg3",
                "arg4 with spaces"
            ]
        );
        assert_eq!(actual, &expected);
    }

    #[test]
    fn test_execute_alias_multiarg_asis() {
        let mut ctx = test_utils::make_sh_ctx();
        let mut ctx = ctx.get_shell_ctx();

        ctx.shell.register_builtins(ctx.inner).unwrap();

        // Test ASIS (default)
        Kush::execute(
            &mut ctx,
            "alias testalias2 \"test_cmd literalarg1 $2 ${3-}\"",
        )
        .unwrap();
        Kush::execute(&mut ctx, "testalias2   arg1 arg2 arg3 \"arg4 with spaces\"").unwrap();
        let actual = ctx.inner;
        let expected = format!(
            "{:?}",
            [
                "test_cmd",
                "literalarg1",
                "arg2",
                "arg3",
                "arg4 with spaces"
            ]
        );
        assert_eq!(actual, &expected);
    }

    #[test]
    fn test_execute_alias_nested_variable() {
        let cmd = "seta $1 \"${$1 ?}\"";
        let alias = format!(
            "alias makesaved \"{}\"",
            cmd.replace("\\", "\\\\").replace("\"", "\\\"")
        );

        let mut ctx = test_utils::make_sh_ctx();
        let mut ctx = ctx.get_shell_ctx();

        ctx.shell.register_builtins(ctx.inner).unwrap();
        Kush::execute(&mut ctx, &alias).unwrap();
        //TODO: and assert what?
    }

    #[test]
    fn test_kush_create_alias() {
        let mut ctx = test_utils::make_sh_ctx();
        let mut ctx = ctx.get_shell_ctx();

        ctx.shell.register_builtins(ctx.inner).unwrap();
        let command_string = "alias testalias \"test literalarg1 $2 ${3-}\"";
        Kush::execute(&mut ctx, command_string).unwrap();

        assert!(ctx.shell.aliases.contains_key("testalias"));

        let argv = vec![
            (Argv::unquoted("test"), Token::Literal("test".into())),
            (
                Argv::unquoted("literalarg1"),
                Token::Literal("literalarg1".into()),
            ),
            (
                Argv::unquoted("$2"),
                Token::Variable(Variable::ArgvExact(2), VariableFlag::default()),
            ),
            (
                Argv::unquoted("${3-}"),
                Token::Variable(
                    Variable::ArgvMulti(ArgvRange::start(3)),
                    VariableFlag::default(),
                ),
            ),
        ];

        let pipelines = Pipeline {
            pipeline: vec![PipelineCommand {
                argv,
                op: CommandBehavior::ExitOutput,
            }],
        };
        let command_string = "test literalarg1 $2 ${3-}";
        let expected = KushAlias::new(vec![pipelines], command_string.to_string(), String::new());

        let actual = ctx.shell.aliases.get("testalias").unwrap();
        assert_eq!(actual, &expected);
    }

    #[test]
    fn test_execute_alias_quoted_all_args() {
        let mut ctx = test_utils::make_sh_ctx();
        let mut ctx = ctx.get_shell_ctx();

        ctx.shell.register_builtins(ctx.inner).unwrap();

        //Register alias
        Kush::execute(&mut ctx, "alias seta \"set ${* q}\"").unwrap();

        //Assert it was created correctly
        let seta = ctx.shell.aliases.get("seta").unwrap();
        let literal = Token::Literal("set".to_owned());
        let position = Variable::ArgvMulti(ArgvRange::start(1));
        let variable = Token::Variable(position, VariableFlag::Quoted);
        let argv = vec![
            (Argv::unquoted("set"), literal),
            (Argv::unquoted("${* q}"), variable),
        ];

        let pipelines = vec![Pipeline {
            pipeline: vec![PipelineCommand {
                argv,
                op: CommandBehavior::default(),
            }],
        }];
        let expected = KushAlias {
            description: String::new(),
            command_string: "set ${* q}".into(),
            pipelines,
        };
        assert_eq!(seta, &expected);

        //Execute the alias
        let var_help = "Configuration file version (used to upgrade settings) 0: first run, or previous start was <2.4.1  Later, it's overridden by config.cfg, version ranges are defined in config_update.cfg";
        let cmd_line = format!("seta g_configversion 42     \"{}\"", var_help);
        if let Err(e) = Kush::execute(&mut ctx, &cmd_line) {
            panic!("{e}");
        }

        let v = ctx
            .shell
            .get_variable_provider()
            .get_variable(ctx.inner, "g_configversion")
            .unwrap();

        assert_eq!(v, "42");
    }
}
