use super::pipeline::Pipeline;

/// An alias can be a combination of several chains of pipes
#[derive(Debug, PartialEq)]
pub struct KushAlias {
    pub description: String,
    pub command_string: String,
    pub pipelines: Vec<Pipeline>,
}

impl KushAlias {
    pub fn new(pipelines: Vec<Pipeline>, command_string: String, description: String) -> Self {
        Self {
            pipelines,
            command_string,
            description,
        }
    }
    /* pub fn build_pipeline<CP: CommandProvider>(
        &self,
        ctx: &ShellCtx<CP>,
        alias_argv: &[String],
    ) -> Result<Vec<CommandPipeline>, KushError> {
        let mut pipelines = vec![];

        for c in &self.parsed_commands {
            let mut cmd_argv = vec![];

            for a in &c.argv {
                let eval = ctx.shell.eval_token(&ctx.inner, a, alias_argv, 0)?;
                cmd_argv.exte(eval.join(" "));
            }
            let alias_argv = cmd_argv.join(" ");
            let alias_argv = lang::parse_argv_line(&alias_argv)?;
            pipelines.append(&mut CommandPipeline::create(alias_argv)?);
        }

        Ok(pipelines)
    }


    pub fn parse(command_string: String, description: String) -> Result<Self, KushError> {
        // Tokenize the command string, command string is just for easy to what the token tree represents
        let parsed_cmds = kush_lang::parse_argv_line(&command_string)?;
        let mut pipelines = vec![];
        let mut current_pipeline = vec![];
        for parsed_cmd in parsed_cmds {
            for parsed_argv in parsed_cmd.argv {
                let token = kush_lang::parse_tokens(&parsed_argv)?;
                current_pipeline.push(token);
            }

            if let CommandBehavior::ExitOutput = parsed_cmd.op {
                let cmd = CommandPipeline {
                    pipeline: current_pipeline,
                };
                pipelines.push(cmd);
                current_pipeline = vec![];
            }
        }
        if !current_pipeline.is_empty() {
            return Err(KushError::ParserError(format!(
                "Unterminated pipe in '{command_string}'"
            )));
        }

        Ok(Self {
            command_string,
            pipelines,
            description,
        })
    }
     */
}
