use kush_lang::parsed::{argv::Argv, tokenized::Token, CommandBehavior};

use crate::KushError;

/// A pipeline is a chain of commands (each token), where output of previous is input to next or last sends output to stdout
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Pipeline {
    /// Each command to pipe until last which sends to stdout
    pub pipeline: Vec<PipelineCommand>,
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct PipelineCommand {
    pub argv: Vec<(Argv, Token)>,
    /// The operation for argv (to pipe, or not to pipe)
    pub op: CommandBehavior,
}

impl Pipeline {
    pub fn parse(line: &str) -> Result<Option<Vec<Pipeline>>, KushError> {
        let line = super::strip_line(line);
        if line.is_empty() {
            return Ok(None);
        }

        // Tokenize the command string, command string is just for easy to what the token tree represents
        let parsed_cmds = kush_lang::parse_argv_line(&line)?;
        let mut pipelines = vec![];
        let mut current_cmd = vec![];
        for parsed_argv in parsed_cmds {
            let op = parsed_argv.op;
            let mut argv = vec![];
            for v in parsed_argv.argv {
                let token = kush_lang::parse_tokens(v.value())?;
                argv.push((v, token));
            }
            current_cmd.push(PipelineCommand { argv, op });

            if let CommandBehavior::ExitOutput = op {
                let pipeline = current_cmd;
                current_cmd = vec![];
                let cmd = Pipeline { pipeline };
                pipelines.push(cmd);
            }
        }
        if !current_cmd.is_empty() {
            return Err(KushError::ParserError(format!(
                "Unterminated pipe in '{line}'"
            )));
        }
        if pipelines.is_empty() {
            return Ok(None);
        }

        Ok(Some(pipelines))
    }
}

#[cfg(test)]
mod tests {
    use crate::shell::pipeline::{Pipeline, PipelineCommand};
    use kush_lang::parsed::{
        argv::Argv,
        tokenized::{Token, Variable, VariableFlag},
        CommandBehavior,
    };

    #[test]
    fn test_pipeline_parse() {
        let command_string = "seta $1 \"${$1 ?}\"";
        // Create the alias
        //Kush::execute(ctx, alias).unwrap();
        let result = Pipeline::parse(command_string);
        let actual = match result {
            Ok(Some(a)) => a,
            _ => panic!("Bad result from parse: {:#?}", result),
        };

        // makesaved test_var
        // should evaluate to
        // seta test_var "${test_var ?}"
        // should evaluate to
        // set test_var "${testvar ?}"
        // then
        // set test_var ""
        // With argv[2] present but empty (vs no argv[2] at all)

        // argv[0]: seta
        // argv[1]: \"${$1 ?}\"
        let argv = vec![
            (Argv::unquoted("seta"), Token::Literal("seta".to_string())),
            (
                Argv::unquoted("$1"),
                Token::Variable(Variable::ArgvExact(1), VariableFlag::default()),
            ),
            (
                Argv::quoted("${$1 ?}"),
                Token::Variable(
                    Variable::Eval(vec![Token::Variable(
                        Variable::ArgvExact(1),
                        VariableFlag::default(),
                    )]),
                    VariableFlag::Optional,
                ),
            ),
        ];

        let expected = vec![Pipeline {
            pipeline: vec![PipelineCommand {
                argv,
                op: CommandBehavior::ExitOutput,
            }],
        }];

        assert_eq!(actual, expected);
    }
}
