use crate::{
    command::{CommandExecutor, CommandProvider},
    error::KushError,
    ShellCtx,
};
use clap::Parser;

#[derive(clap::Parser)]
#[clap(name = "echo", about = "echo something to console")]
pub struct EchoCmd {
    args: Vec<String>,
}

impl<CP: CommandProvider> CommandExecutor<CP> for EchoCmd {
    fn execute(ctx: &mut ShellCtx<CP>, argv: &[String]) -> Result<(), KushError> {
        let args = EchoCmd::try_parse_from(argv)?;
        let line = args.args.join(" ");

        ctx.stdio.println(line)?;

        Ok(())
    }
}
