use crate::{
    command::{CommandExecutor, CommandProvider},
    error::KushError,
    variable::VariableProvider,
    ShellCtx,
};
use clap::Parser;

#[derive(clap::Parser)]
#[clap(name = "set", allow_hyphen_values = true, about = "set a variable")]
pub struct SetCmd {
    name: String,
    value: String,
    description: Option<String>,
}

impl<CP: CommandProvider> CommandExecutor<CP> for SetCmd {
    fn execute(ctx: &mut ShellCtx<CP>, argv: &[String]) -> Result<(), KushError> {
        //Hmm, a predicament...  In this one command, I need to mutate shell vars, and I need to be able to respond if successful or failure
        // Or, I could return exec context back, with the changed variables.
        let args = SetCmd::try_parse_from(argv)?;
        ctx.shell
            .get_variable_provider_mut()
            .set_variable(ctx.inner, args.name, args.value)?;

        Ok(())
    }
}

#[derive(clap::Parser)]
#[clap(name = "unset", about = "remove a variable")]
pub struct UnsetCmd {
    name: String,
}

impl<CP: CommandProvider> CommandExecutor<CP> for UnsetCmd {
    fn execute(ctx: &mut ShellCtx<CP>, argv: &[String]) -> Result<(), KushError> {
        //Hmm, a predicament...  In this one command, I need to mutate shell vars, and I need to be able to respond if successful or failure
        // Or, I could return exec context back, with the changed variables.
        let args = UnsetCmd::try_parse_from(argv)?;
        if ctx
            .shell
            .get_variable_provider()
            .contains_variable(ctx.inner, &args.name)
        {
            ctx.shell
                .get_variable_provider_mut()
                .delete_variable(ctx.inner, &args.name)?;
        }

        Ok(())
    }
}
