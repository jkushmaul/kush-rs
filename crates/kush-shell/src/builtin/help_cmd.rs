use crate::{
    command::{CommandExecutor, CommandProvider},
    error::KushError,
    ShellCtx,
};

#[derive(clap::Parser)]
#[clap(name = "help", about = "available commands")]
pub struct HelpCmd;

impl<CP: CommandProvider> CommandExecutor<CP> for HelpCmd {
    fn execute(ctx: &mut ShellCtx<CP>, _argv: &[String]) -> Result<(), KushError> {
        let mut helps: Vec<(String, String)> = vec![];

        let cp = ctx.shell.get_command_provider_mut();

        for name in cp.command_keys() {
            if let Some(c) = cp.get_command(ctx.inner, &name) {
                let help = c.get_about().to_owned();
                helps.push((name, help));
            }
        }

        if !helps.is_empty() {
            helps.sort_by(|a, b| a.0.cmp(&b.0));
            let mut help_lines = vec![];
            help_lines.push("Commands:".to_owned());
            for (n, h) in helps {
                help_lines.push(format!("{}\t\t{}", n, h));
            }
            ctx.stdio.println(help_lines.join("\n"))?;
        } else {
            ctx.stdio.println("No commands exist".to_owned())?;
        }

        Ok(())
    }
}
