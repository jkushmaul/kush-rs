use crate::{
    command::{CommandExecutor, CommandProvider},
    error::KushError,
    ShellCtx,
};
use clap::Parser;
use std::io::BufRead;

#[derive(clap::Parser)]
#[clap(
    name = "grep",
    allow_hyphen_values = true,
    about = "search stdin for text"
)]
pub struct GrepCmd {
    #[arg(short = 'v', long = "inverse", default_value = "false")]
    inverse: bool,
    pattern: String,
}

impl<CP: CommandProvider> CommandExecutor<CP> for GrepCmd {
    fn execute(ctx: &mut ShellCtx<CP>, argv: &[String]) -> Result<(), KushError> {
        let args = GrepCmd::try_parse_from(argv)?;

        let stdin = ctx.stdio.stdin.take();
        if let Some(mut r) = stdin {
            loop {
                let mut line = String::new();
                let x = r.read_line(&mut line)?;
                if x == 0 {
                    break;
                }
                if line.contains(&args.pattern) {
                    ctx.stdio.println(line)?;
                }
            }
        }

        Ok(())
    }
}
