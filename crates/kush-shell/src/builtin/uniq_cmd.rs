use crate::{
    command::{CommandExecutor, CommandProvider},
    error::KushError,
    ShellCtx,
};
use clap::Parser;
use std::{
    collections::{hash_map::Entry, HashMap},
    io::BufRead,
};

#[derive(clap::Parser)]
#[clap(name = "uniq", allow_hyphen_values = true, about = "reduce to uniques")]
pub struct UniqCmd {
    #[arg(short = 'c', long = "count", default_value = "false")]
    counts: bool,
}

impl<CP: CommandProvider> CommandExecutor<CP> for UniqCmd {
    fn execute(ctx: &mut ShellCtx<CP>, argv: &[String]) -> Result<(), KushError> {
        let args = UniqCmd::try_parse_from(argv)?;

        let mut v: HashMap<String, usize> = HashMap::new();
        let mut stdin = ctx.stdio.stdin.take();
        if let Some(r) = stdin.as_mut() {
            loop {
                let mut line = String::new();
                let x = r.read_line(&mut line)?;
                if x == 0 {
                    break;
                }
                match v.entry(line) {
                    Entry::Occupied(mut o) => {
                        *o.get_mut() += 1;
                    }
                    Entry::Vacant(v) => {
                        v.insert(0);
                    }
                }
            }
        }

        for s in v {
            if args.counts {
                ctx.stdio.println(format!("{}\t{}", s.1, s.0))?;
            } else {
                ctx.stdio.println(s.0)?;
            }
        }

        Ok(())
    }
}
