use crate::{
    command::{CommandExecutor, CommandProvider},
    error::KushError,
    ShellCtx,
    variable::VariableProvider,
};
use clap::Parser;

#[derive(clap::Parser)]
#[clap(name = "env", about = "print env vars")]
pub struct EnvCmd {
    prefix: Option<String>,
}
pub const EMPTY_STR: &str = "";

impl<CP: CommandProvider> CommandExecutor<CP> for EnvCmd {
    fn execute(ctx: &mut ShellCtx<CP>, argv: &[String]) -> Result<(), KushError> {
        let args = EnvCmd::try_parse_from(argv)?;
        let prefix = match &args.prefix {
            Some(s) => s,
            None => EMPTY_STR,
        };
        let vp = ctx.shell.get_variable_provider();

        let mut v: Vec<_> = vp
            .variable_keys(ctx.inner)
            .filter(|k| prefix.is_empty() || k.starts_with(prefix))
            .filter_map(|k| vp.get_variable(ctx.inner, k).map(|v| (k, v)))
            .map(|(k, v)| format!("{}:   {}", k, v))
            .collect();
        v.sort();

        ctx.stdio.println(v.join("\n"))?;

        Ok(())
    }
}
