//! Convenience builtins for basic shell operation

use crate::{
    command::{clap::BasicClapCommandBuilder, CommandProvider, CommandType},
    error::KushError,
};

mod alias_cmd;
mod echo_cmd;
mod env_cmd;
mod grep_cmd;
mod help_cmd;
mod set_cmd;
mod sort_cmd;
mod uniq_cmd;

/// Provides a set of useful commands
pub fn all_builtin<CP: CommandProvider>() -> Result<Vec<CommandType<CP>>, KushError> {
    // Apologies to the greater community.
    let alias_cmd = alias_cmd::AliasCmd::build_basic_command();
    let unalias_cmd = alias_cmd::UnaliasCmd::build_basic_command();
    let set_cmd = set_cmd::SetCmd::build_basic_command();
    let unset_cmd = set_cmd::UnsetCmd::build_basic_command();
    let help_cmd = help_cmd::HelpCmd::build_basic_command();

    let echo_cmd = echo_cmd::EchoCmd::build_basic_command();
    let env_cmd = env_cmd::EnvCmd::build_basic_command();
    let grep_cmd = grep_cmd::GrepCmd::build_basic_command();
    let sort_cmd = sort_cmd::SortCmd::build_basic_command();
    let uniq_cmd = uniq_cmd::UniqCmd::build_basic_command();

    let v: Vec<CommandType<CP>> = vec![
        //system cmds
        alias_cmd,
        unalias_cmd,
        set_cmd,
        unset_cmd,
        help_cmd,
        // basic cmds
        echo_cmd,
        env_cmd,
        grep_cmd,
        sort_cmd,
        uniq_cmd,
    ];

    Ok(v)
}
