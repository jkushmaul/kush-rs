use crate::{
    command::{CommandExecutor, CommandProvider},
    error::KushError,
    ShellCtx,
};
use clap::Parser;

#[derive(clap::Parser)]
#[clap(
    name = "alias",
    allow_hyphen_values = true,
    about = "create an alias of other commands"
)]
pub struct AliasCmd {
    name: String,
    #[arg(default_value_t = String::new())]
    script: String,
    description: Option<String>,
}

impl<CP: CommandProvider> CommandExecutor<CP> for AliasCmd {
    fn execute(ctx: &mut ShellCtx<CP>, argv: &[String]) -> Result<(), KushError> {
        if argv.is_empty() {
            for (k, v) in ctx.shell.get_aliases() {
                ctx.stdio.println(format!("{}: {}", k, v.command_string))?;
            }
        } else {
            let args = AliasCmd::try_parse_from(argv)?;
            ctx.shell.register_alias(
                args.name,
                args.script,
                args.description.unwrap_or_default(),
            )?;
        }
        Ok(())
    }
}

#[derive(clap::Parser)]
#[clap(
    name = "unalias",
    allow_hyphen_values = true,
    about = "remove an alias"
)]
pub struct UnaliasCmd {
    name: String,
}

impl<CP: CommandProvider> CommandExecutor<CP> for UnaliasCmd {
    fn execute(ctx: &mut ShellCtx<CP>, argv: &[String]) -> Result<(), KushError> {
        let args = UnaliasCmd::try_parse_from(argv)?;
        ctx.shell.remove_alias(args.name);
        Ok(())
    }
}
