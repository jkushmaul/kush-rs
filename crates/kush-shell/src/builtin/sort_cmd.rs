use crate::{
    command::{CommandExecutor, CommandProvider},
    error::KushError,
    ShellCtx,
};
use clap::Parser;
use regex::Regex;
use std::{cmp::Ordering, io::BufRead};

#[derive(clap::Parser)]
#[clap(name = "sort", allow_hyphen_values = true, about = "sort stdin")]
pub struct SortCmd {
    #[arg(short = 'v', long = "reverse", default_value = "false")]
    reverse: bool,
    #[arg(short = 'n', long = "numeric", default_value = "false")]
    numeric: bool,
}

impl<CP: CommandProvider> CommandExecutor<CP> for SortCmd {
    fn execute(ctx: &mut ShellCtx<CP>, argv: &[String]) -> Result<(), KushError> {
        let args = SortCmd::try_parse_from(argv)?;
        let mut v: Vec<String> = Vec::new();
        let mut stdin = ctx.stdio.stdin.take();
        if let Some(r) = stdin.as_mut() {
            loop {
                let mut line = String::new();
                let x = r.read_line(&mut line)?;
                if x == 0 {
                    break;
                }
                v.push(line);
            }
        }

        if args.numeric {
            ctx.stdio.println("numeric sort:".to_owned())?;
        }

        let numeric_re = Regex::new(r"^[+-]?([\d]+(\.[\d]+)?)").unwrap();
        v.sort_by(|a, b| {
            if args.numeric {
                let mut aa: Option<f64> = None;
                let mut bb: Option<f64> = None;

                if let Some(a) = numeric_re.captures(a) {
                    if let Some(a) = a.get(1) {
                        if let Ok(a) = a.as_str().parse::<f64>() {
                            aa = Some(a);
                        }
                    }
                }
                if let Some(b) = numeric_re.captures(b) {
                    if let Some(b) = b.get(1) {
                        if let Ok(b) = b.as_str().parse::<f64>() {
                            bb = Some(b);
                        }
                    }
                }

                match (aa, bb) {
                    (None, None) => a.cmp(b),
                    (None, Some(_)) => Ordering::Greater,
                    (Some(_), None) => Ordering::Less,
                    (Some(a), Some(b)) => {
                        if a < b {
                            Ordering::Less
                        } else if a > b {
                            Ordering::Greater
                        } else {
                            Ordering::Equal
                        }
                    }
                }
            } else {
                a.cmp(b)
            }
        });

        if args.reverse {
            v.reverse();
        }

        ctx.stdio.println(v.join("\n"))?;

        Ok(())
    }
}
