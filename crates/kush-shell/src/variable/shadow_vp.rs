//! Drop dead simple hashmap implementation

use super::VariableProvider;
use crate::error::KushError;
use std::collections::{hash_map::Entry, HashMap};

/// Allows creating a set of shadow vars atop existing provider
/// eg) temp vars, stack vars, etc.

#[derive(Default)]
pub struct ShadowVariableProvider<VP: VariableProvider> {
    inner: VP,
    temp: HashMap<String, String>,
}

impl<VP: VariableProvider> ShadowVariableProvider<VP> {
    /// Get a ref to inner
    pub fn inner(&self) -> &VP {
        &self.inner
    }
    /// Get a mutable ref to inner
    pub fn inner_mut(&mut self) -> &mut VP {
        &mut self.inner
    }

    /// Start fresh
    pub fn reset(&mut self) {
        if !self.temp.is_empty() {
            self.temp.clear();
            self.invalidate();
        }
    }

    /// Set a temporary var
    pub fn set_shadowed(&mut self, name: String, value: String) {
        let modified = match self.temp.entry(name) {
            Entry::Occupied(mut o) => {
                if o.get() != &value {
                    o.insert(value);
                    true
                } else {
                    false
                }
            }
            Entry::Vacant(v) => {
                v.insert(value);
                true
            }
        };
        if modified {
            self.invalidate();
        }
    }

    /// Quickly construct a wrapped VP with shadowed vars
    pub fn new(base: VP) -> Self {
        Self {
            inner: base,
            temp: Default::default(),
        }
    }
}

impl<VP: VariableProvider> VariableProvider for ShadowVariableProvider<VP> {
    //Seems like there might be a better way to "pass through" this in constraint above...?
    type T = VP::T;

    fn set_variable(
        &mut self,
        state: &mut Self::T,
        name: String,
        value: String,
    ) -> Result<(), KushError> {
        self.inner.set_variable(state, name, value)
    }

    fn get_variable(&self, state: &Self::T, name: &str) -> Option<&str> {
        self.temp
            .get(name)
            .map(|s| s.as_str())
            .or_else(|| self.inner.get_variable(state, name))
    }

    fn variable_keys<'a>(&'a self, state: &Self::T) -> impl Iterator<Item = &'a str> {
        self.temp
            .keys()
            .map(|s| s.as_str())
            .chain(self.inner.variable_keys(state))
    }

    fn delete_variable(&mut self, state: &mut Self::T, name: &str) -> Result<(), KushError> {
        if self.temp.contains_key(name) {
            let _ = self.temp.remove(name);
            self.inner.invalidate();
        }

        self.inner.delete_variable(state, name)?;

        Ok(())
    }

    fn contains_variable(&self, state: &Self::T, name: &str) -> bool {
        self.temp.contains_key(name) || self.inner.contains_variable(state, name)
    }

    fn last_modified(&self) -> usize {
        self.inner.last_modified()
    }

    fn invalidate(&mut self) {
        self.inner.invalidate();
    }
}

#[cfg(test)]
mod tests {
    use crate::variable::{MapVariableProvider, VariableProvider};

    use super::ShadowVariableProvider;

    #[test]
    fn test_shadow_reset() {
        let mut state = String::new();
        let state = &mut state;
        let mut inner = MapVariableProvider::<String>::default();
        let inner_var = "INNER";
        let inner_val = "INNER_VALUE";
        inner
            .set_variable(state, inner_var.to_string(), inner_val.to_string())
            .unwrap();

        let mut shadowed = ShadowVariableProvider::new(inner);
        shadowed.set_shadowed("0".to_string(), "ZERO".to_string());
        let actual = shadowed.get_variable(state, "0").unwrap();
        assert_eq!(actual, "ZERO");
        shadowed.set_shadowed(inner_var.to_string(), "TEMP".to_string());
        let actual = shadowed.get_variable(state, inner_var).unwrap();
        assert_eq!(actual, "TEMP");

        shadowed.reset();
        let actual = shadowed.get_variable(state, inner_var).unwrap();
        assert_eq!(actual, inner_val);
    }
}
