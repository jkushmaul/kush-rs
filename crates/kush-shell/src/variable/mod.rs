//! Variable related things

mod map_vp;
mod shadow_vp;

pub use map_vp::MapVariableProvider;
pub use shadow_vp::ShadowVariableProvider;

use crate::KushError;

/// A variable provider allows customization to the registration, setting and getting of variables
pub trait VariableProvider {
    /// The shell state type
    type T;

    /// Change a variable value
    /// state: The shell state
    /// name: The name of the variable
    /// value: String
    fn set_variable(
        &mut self,
        state: &mut Self::T,
        name: String,
        value: String,
    ) -> Result<(), KushError>;

    /// Allows for fast caching to avoid cloning unless mutated
    fn last_modified(&self) -> usize;

    /// Increment modification directly
    fn invalidate(&mut self);

    /// Remove a variable
    fn delete_variable(&mut self, state: &mut Self::T, name: &str) -> Result<(), KushError>;

    /// Get a variable value by name
    fn get_variable(&self, state: &Self::T, name: &str) -> Option<&str>;

    /// Get all variables + values
    fn variable_keys(&self, state: &Self::T) -> impl Iterator<Item = &str>;

    /// Ask if the variable is already set
    fn contains_variable(&self, state: &Self::T, name: &str) -> bool;
}
