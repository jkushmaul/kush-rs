//! Drop dead simple hashmap implementation

use crate::error::KushError;
use std::{
    collections::{hash_map::Entry, HashMap},
    marker::PhantomData,
};

use super::VariableProvider;

/// Basic hashmap functionality provided
pub struct MapVariableProvider<T> {
    variables: HashMap<String, String>,

    phantom: PhantomData<T>,
    last_modified: usize,
}

impl<T> Default for MapVariableProvider<T> {
    fn default() -> Self {
        Self {
            variables: Default::default(),
            phantom: Default::default(),
            last_modified: 0,
        }
    }
}

impl<T> VariableProvider for MapVariableProvider<T> {
    //Seems like there might be a better way to "pass through" this in constraint above...?
    type T = T;

    fn set_variable(
        &mut self,
        _: &mut Self::T,
        name: String,
        value: String,
    ) -> Result<(), KushError> {
        let modified = match self.variables.entry(name) {
            Entry::Occupied(mut o) => {
                if o.get() != &value {
                    o.insert(value);
                    true
                } else {
                    false
                }
            }
            Entry::Vacant(v) => {
                v.insert(value);
                true
            }
        };
        if modified {
            self.invalidate();
        }

        Ok(())
    }

    fn get_variable(&self, _: &T, name: &str) -> Option<&str> {
        self.variables.get(name).map(|s| s.as_str())
    }

    fn variable_keys<'a>(&'a self, _: &T) -> impl Iterator<Item = &'a str> {
        self.variables.keys().map(|s| s.as_str())
    }

    fn delete_variable(&mut self, _: &mut T, name: &str) -> Result<(), KushError> {
        if self.variables.remove(name).is_some() {
            self.invalidate();
        }
        Ok(())
    }

    fn contains_variable(&self, _: &Self::T, name: &str) -> bool {
        self.variables.contains_key(name)
    }

    fn last_modified(&self) -> usize {
        self.last_modified
    }
    fn invalidate(&mut self) {
        self.last_modified += 1;
    }
}
