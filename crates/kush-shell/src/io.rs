use crate::KushError;
use futures::channel::mpsc::{self, Receiver, Sender};
use std::io::{BufRead, BufReader, BufWriter, Read, Write};

type KstdioStdin = Box<dyn BufRead + Send + Sync>;
type KstdioStdout = Box<dyn Write + Send + Sync>;

/// Replicating how stdin/stdout might work as name suggests
/// Pipes would be better for me but rust doesn't have super cool support for that yet.
pub struct KStdio {
    /// A stdin rx if any
    pub stdin: Option<KstdioStdin>,
    /// A stdout tx to send output
    pub stdout: KstdioStdout,
}

impl KStdio {
    /// A wrapper around try_send which also returns KushError enum type
    /// Ideally this can become format! like println!
    pub fn println(&mut self, line: String) -> Result<(), KushError> {
        self.stdout.write_all(line.as_bytes())?;
        self.stdout.write_all(b"\n")?;
        self.stdout.flush()?;

        Ok(())
    }
    /// A wrapper around Option<Receiver.try_send>
    /// It does not quite follow the same signature that stdin.read_line would with &mut buf
    /// because the allocation for a new String is already done.
    pub fn readln(&mut self) -> Result<Option<String>, KushError> {
        let stdin = match self.stdin.as_mut() {
            Some(s) => s,
            None => return Ok(None),
        };
        let mut line = String::new();
        let len = stdin.read_line(&mut line)?;
        if len == 0 {
            return Ok(None);
        }

        Ok(Some(line))
    }

    /// Construct a new stdio
    pub fn new(stdin: Option<KstdioStdin>, stdout: KstdioStdout) -> Self {
        Self { stdin, stdout }
    }

    /// Output only, returns the read receiver and the kstdio for output
    pub fn new_channel() -> (ReadReceiver, Self) {
        let (tx, rx) = mpsc::channel(1024);
        let tx = WriteSender::from(tx);
        let rx = ReadReceiver::from(rx);
        (rx, Self::new(None, Box::new(tx)))
    }

    /// Construct a new stdio from std::io
    pub fn new_stdio() -> Self {
        let stdin = std::io::stdin();
        let stdin = BufReader::new(stdin);
        let stdout = BufWriter::new(std::io::stdout());
        KStdio::new(Some(Box::new(stdin)), Box::new(stdout))
    }

    /// Not very helpful but allows you to being a new pipe
    pub fn pipe(&mut self, stdout_tx: KstdioStdout) -> KstdioStdout {
        // Throw it away
        std::mem::replace(&mut self.stdout, stdout_tx)
    }
}

/// Write impl for `Sender<Vec<u8>>`
#[derive(Clone)]
pub struct WriteSender {
    sender: Sender<Vec<u8>>,
}
impl From<Sender<Vec<u8>>> for WriteSender {
    fn from(sender: Sender<Vec<u8>>) -> Self {
        Self { sender }
    }
}
impl std::io::Write for WriteSender {
    fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
        self.sender
            .try_send(buf.to_vec())
            .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e))?;
        Ok(buf.len())
    }

    fn flush(&mut self) -> std::io::Result<()> {
        Ok(())
    }
}

/// Uses a Receiver of buffers to relay data to simulate pipes
pub struct ReadReceiver {
    peekable: Option<Vec<u8>>,
    receiver: Receiver<Vec<u8>>,
}
impl From<Receiver<Vec<u8>>> for ReadReceiver {
    fn from(receiver: Receiver<Vec<u8>>) -> Self {
        Self {
            receiver,
            peekable: None,
        }
    }
}

impl Read for ReadReceiver {
    fn read(&mut self, mut buf: &mut [u8]) -> std::io::Result<usize> {
        let mut size_read: usize = 0;

        if let Some(peekable) = &mut self.peekable {
            if peekable.len() > buf.len() {
                buf.copy_from_slice(&peekable[0..buf.len()]);
                //Peekable doesn't go away it just shrinks
                *peekable = peekable[buf.len()..].to_vec();
                return Ok(buf.len());
            }

            // All of peekable is consumed and it goes away
            buf[0..peekable.len()].copy_from_slice(peekable);
            size_read += peekable.len();
            buf = &mut buf[peekable.len()..];
            self.peekable = None;
            //And more can be read
        }

        loop {
            let r = match self.receiver.try_next() {
                Ok(Some(r)) => r,
                Ok(None) => break,
                Err(_) => {
                    break;
                }
            };
            if r.len() > buf.len() {
                //Take what you can and leave the rest in peek
                buf.copy_from_slice(&r[0..buf.len()]);
                self.peekable = Some(Vec::from(&r[buf.len()..]));
                size_read += buf.len();
                break;
            }
            //transfer into buf and continue
            buf[0..r.len()].copy_from_slice(&r);
            size_read += r.len();
            buf = &mut buf[r.len()..];
        }

        if size_read == 0 {
            return Err(std::io::Error::new(
                std::io::ErrorKind::UnexpectedEof,
                "End of file",
            ));
        }
        Ok(size_read)
    }
}
