//! Command related things

pub mod clap;
pub mod map_cp;
use crate::{ShellCtx, variable::VariableProvider, KushError};

/// A trait that indicates a type has an execute() fn
pub trait CommandExecutor<CP: CommandProvider> {
    /// Get the execute
    fn execute(state: &mut ShellCtx<CP>, argv: &[String]) -> Result<(), KushError>;
}

/// Basic command storage
pub struct CommandType<CP: CommandProvider> {
    name: String,
    help: String,
    about: String,
    execute: fn(&mut ShellCtx<CP>, argv: &[String]) -> Result<(), KushError>,
}

impl<CP: CommandProvider> Clone for CommandType<CP> {
    fn clone(&self) -> Self {
        Self {
            name: self.name.clone(),
            help: self.help.clone(),
            about: self.about.clone(),
            execute: self.execute,
        }
    }
}

impl<CP: CommandProvider> CommandType<CP> {
    /// Pass a closure as execute
    pub fn with_execute(
        name: String,
        help: String,
        about: String,
        execute: fn(&mut ShellCtx<CP>, argv: &[String]) -> Result<(), KushError>,
    ) -> Self {
        Self {
            name,
            help,
            about,
            execute,
        }
    }

    /// Used to return the execute function for a command
    pub fn get_execute(&self) -> fn(&mut ShellCtx<CP>, argv: &[String]) -> Result<(), KushError> {
        self.execute
    }

    /// Construct from trait implementation
    pub fn new<P: CommandExecutor<CP>>(name: String, help: String, about: String) -> Self {
        Self {
            name,
            help,
            about,
            execute: P::execute,
        }
    }
    /// Get the name
    pub fn get_name(&self) -> &str {
        &self.name
    }

    /// Get the help
    pub fn get_help(&self) -> &str {
        &self.help
    }

    /// Get the about
    pub fn get_about(&self) -> &str {
        &self.about
    }
}

/// CommandProvider's job is to store commands - allow getting and setting based on it's specific
/// logic vs using some generic logic like simple hashmap.
pub trait CommandProvider {
    /// The shell state type
    type T;
    /// The variable provider type
    type VP: VariableProvider<T = Self::T>;

    /// All commands registered
    fn command_keys(&self) -> Vec<String>;

    /// Check if command exists
    fn contains_command(&self, state: &Self::T, name: &str) -> bool;

    /// Get a command by name
    fn get_command(&self, state: &Self::T, name: &str) -> Option<&CommandType<Self>>
    where
        Self: Sized;

    /// Remove a command if it exists
    fn remove_command(&mut self, state: &mut Self::T, name: &str) -> Option<CommandType<Self>>
    where
        Self: Sized;

    /// Used to register a command for use
    fn register_command(
        &mut self,
        state: &Self::T,
        command: CommandType<Self>,
    ) -> Result<(), KushError>
    where
        Self: Sized;
}

// I feel the need to have a "SystemCommand" type - that is the same thing, but, accepts the shell itself as an argument.
// This is difficult.
