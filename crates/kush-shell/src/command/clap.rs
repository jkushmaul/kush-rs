//! Clap is such a common command line argument crate that I offer convenience traits here
//! I understand that this is disgusting...

use super::{CommandExecutor, CommandProvider, CommandType};

/// Basic command builder for clap structs
pub trait BasicClapCommandBuilder<P: clap::Parser + CommandExecutor<CP>, CP: CommandProvider> {
    /// Build a basic command from a clap command - that also implements command executor
    fn build_basic_command() -> CommandType<CP>;
}

impl<P: clap::Parser + CommandExecutor<CP>, CP: CommandProvider> BasicClapCommandBuilder<P, CP>
    for P
{
    fn build_basic_command() -> CommandType<CP> {
        let mut cmd = P::command();
        let name = cmd.get_name().to_string();
        let help = format!("{}", cmd.render_help());
        let about = match cmd.get_about() {
            Some(s) => format!("{}", s),
            None => String::new(),
        };
        CommandType::new::<P>(name, help, about)
    }
}

#[cfg(test)]
mod test {
    use super::BasicClapCommandBuilder;
    use crate::{
        command::{map_cp::HashMapCommandProvider, CommandExecutor, CommandProvider, CommandType},
        ShellCtx,
        variable::MapVariableProvider,
        KushError,
    };
    use clap::Parser;

    #[derive(PartialEq, Parser)]
    #[clap(name = "test")]
    pub struct TestClapCmd {
        args: String,
    }

    impl<CP: CommandProvider> CommandExecutor<CP> for TestClapCmd {
        fn execute(ctx: &mut ShellCtx<CP>, argv: &[String]) -> Result<(), KushError> {
            let args = TestClapCmd::try_parse_from(argv)?;
            ctx.stdio.println(args.args)?;

            Ok(())
        }
    }

    #[test]
    pub fn test_basic_clap() {
        let _: CommandType<HashMapCommandProvider<MapVariableProvider<()>>> =
            TestClapCmd::build_basic_command();
    }
}
