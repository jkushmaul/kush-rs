//! Basic functionality hashmap command provider

use crate::{error::KushError, variable::VariableProvider};
use std::collections::HashMap;

use super::{CommandProvider, CommandType};

/// An example command provider based on a hashmap
pub struct HashMapCommandProvider<VP: VariableProvider> {
    commands: HashMap<String, CommandType<Self>>,
}
impl<VP: VariableProvider> Default for HashMapCommandProvider<VP> {
    fn default() -> Self {
        Self {
            commands: Default::default(),
        }
    }
}

impl<VP: VariableProvider> CommandProvider for HashMapCommandProvider<VP> {
    type VP = VP;
    type T = VP::T;

    fn command_keys(&self) -> Vec<String> {
        self.commands.keys().map(|s| s.to_owned()).collect()
    }

    fn get_command(&self, _state: &Self::T, name: &str) -> Option<&CommandType<Self>> {
        self.commands.get(name)
    }

    fn register_command(
        &mut self,
        state: &Self::T,
        command: CommandType<Self>,
    ) -> Result<(), KushError>
    where
        Self: Sized,
    {
        if self.get_command(state, command.get_name()).is_some() {
            return Err(KushError::DuplicateName(command.get_name().to_owned()));
        }
        self.commands.insert(command.get_name().to_owned(), command);
        Ok(())
    }

    fn contains_command(&self, _: &Self::T, name: &str) -> bool {
        self.commands.contains_key(name)
    }

    fn remove_command(&mut self, _: &mut Self::T, name: &str) -> Option<CommandType<Self>>
    where
        Self: Sized,
    {
        self.commands.remove(name)
    }
}

#[cfg(test)]
mod test {
    use super::HashMapCommandProvider;
    use crate::{
        command::{CommandExecutor, CommandProvider, CommandType},
        test_utils::TestCmd,
        variable::MapVariableProvider,
    };

    #[test]
    pub fn test_get_command() {
        let mut p: HashMapCommandProvider<MapVariableProvider<String>> =
            HashMapCommandProvider::default();

        let c = CommandType::with_execute(
            "test".into(),
            "about".into(),
            "help".into(),
            TestCmd::execute,
        );

        // Create a useable command and register it

        p.commands.insert("test".to_owned(), c);

        let state = String::new();

        let result = p.get_command(&state, "test");
        assert!(result.is_some());
        let result = result.unwrap();

        assert_eq!(result.get_name(), "test");
    }
}
