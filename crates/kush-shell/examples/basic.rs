use kush_shell::{
    command::{map_cp::HashMapCommandProvider, CommandExecutor, CommandProvider},
    variable::MapVariableProvider,
    KStdio, Kush, KushError, ShellCtx, ShellCtxProvider,
};

pub struct ExampleState {}

pub struct ExampleShellCtx {
    shell: Kush<HashMapCommandProvider<MapVariableProvider<ExampleState>>>,
    stdio: KStdio,
    state: ExampleState,
}
impl ShellCtxProvider<'_, HashMapCommandProvider<MapVariableProvider<ExampleState>>>
    for ExampleShellCtx
{
    fn get_shell_ctx(
        &mut self,
    ) -> ShellCtx<'_, HashMapCommandProvider<MapVariableProvider<ExampleState>>> {
        ShellCtx {
            stdio: &mut self.stdio,
            inner: &mut self.state,
            shell: &mut self.shell,
        }
    }
}

pub fn main() {
    let mut state = ExampleState {};
    let stdio = KStdio::new_stdio();

    let variable_provider: MapVariableProvider<ExampleState> = MapVariableProvider::default();
    let command_provider = HashMapCommandProvider::default();
    let mut shell = Kush::new(variable_provider, command_provider).unwrap();

    //If you want to use the builtins, you have to register them manually
    shell.register_builtins(&mut state).unwrap();

    println!("Registering test_alias");

    let mut ctx = ExampleShellCtx {
        state,
        stdio,
        shell,
    };
    let mut ctx = ctx.get_shell_ctx();

    let cmd = "alias if_client \"${* asis}\"";
    println!("Executing {cmd}");
    Kush::execute(&mut ctx, cmd).unwrap();
    let cmd = "if_client    alias qc_cmd_cl     \"echo $$*\"";
    println!("Executing {cmd}");
    Kush::execute(&mut ctx, cmd).unwrap();

    let cmd = "qc_cmd_cl hud clickradar";
    println!("Executing {cmd}");
    Kush::execute(&mut ctx, cmd).unwrap();

    println!("Done");
}

#[derive(clap::Parser)]
#[clap(name = "TestBasicCmd", about = "Echo the args")]
pub struct TestBasicCmd {}

impl<CP: CommandProvider> CommandExecutor<CP> for TestBasicCmd {
    fn execute(_state: &mut ShellCtx<CP>, argv: &[String]) -> Result<(), KushError> {
        println!("TestBasicCmd: {}", argv.join("\n"));
        Ok(())
    }
}
