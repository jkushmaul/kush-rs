use std::io::{BufRead, BufReader};

use criterion::{black_box, criterion_group, criterion_main, BatchSize, Criterion};
use futures::channel::mpsc::{self};
use kush_shell::{
    command::map_cp::HashMapCommandProvider, variable::MapVariableProvider, KStdio, Kush,
    ReadReceiver, ShellCtxProvider, SimpleShellCtx, WriteSender,
};

pub fn make_sh<T: Default>() -> (
    SimpleShellCtx<HashMapCommandProvider<MapVariableProvider<T>>>,
    BufReader<ReadReceiver>,
) {
    let mut inner = T::default();

    let (tx, rx) = mpsc::channel(160_000_000);

    let variable_provider: MapVariableProvider<T> = MapVariableProvider::default();
    let command_provider = HashMapCommandProvider::default();

    let stdout = WriteSender::from(tx);
    let stdio = KStdio::new(None, Box::new(stdout));

    let mut shell = Kush::new(variable_provider, command_provider).unwrap();
    shell.register_builtins(&mut inner).unwrap();

    let rx = BufReader::new(ReadReceiver::from(rx));
    (
        SimpleShellCtx {
            inner,
            stdio,
            shell,
        },
        rx,
    )
}

/*
    single isolated command with no variables
                            time:   [6.6097 µs 6.6135 µs 6.6174 µs]
    Found 682 outliers among 10000 measurements (6.82%)
    1 (0.01%) low mild
    439 (4.39%) high mild
    242 (2.42%) high severe
*/
fn criterion_benchmark_single(c: &mut Criterion) {
    let name = "single isolated command with no variables";
    let (mut ctx, mut stdout_rx) = make_sh::<()>();
    let mut ctx = ctx.get_shell_ctx();

    let mut iteration: usize = 0;
    let mut count = 0;
    c.bench_function(name, |b| {
        b.iter_batched(
            || {
                iteration += 1;
                format!("echo \"Hello {}\"", iteration)
            },
            |cmd_line: String| Kush::execute(black_box(&mut ctx), black_box(&cmd_line)).unwrap(),
            BatchSize::SmallInput,
        );

        //make sure output gets emptied for next
        let mut line = String::new();
        while let Ok(x) = stdout_rx.read_line(&mut line) {
            if x == 0 {
                break;
            }
            assert!(line.starts_with("Hello"));
            count += 1;
        }
    });

    assert!(count > 0);
    assert!(iteration > 0);
}

/*


 I'd like to see these benchmarks:
    * execution
        * single command with max argv
        * single command with single argv
    * pipes
        * 1..2 pipes to single command with single argv
        * 1..4 pipes to single command with single argv
    * alias
        * alias to all of 3 single command variations with ${* q}
        * alias to all of 3 single command variations with ${* asis}
    * evaluation
        * literal string
        * sequence of literal + var named
        * sequence of literal + var argvexact
        * sequence of argv multi
*/

criterion_group! {
        name = benches;
    // This can be any expression that returns a `Criterion` object.
    config = Criterion::default().significance_level(0.1).sample_size(10000);
    targets = criterion_benchmark_single
}
criterion_main!(benches);
